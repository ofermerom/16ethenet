

#include <stdio.h>
#include "hardware.h"

#define MAIN
#include "globals.h"

#include "eeprom.h"

//#define MBDEBUG

//#define TRNDBG


#define STAT_IDEL 0
#define STAT_REC 1
#define STAT_PROC 2
#define STAT_RST 3

uint16_t TcpRstCnt=0;

extern  unsigned char ConfigLowBoudRate;
extern char CfgProgMaode,UartDelayBtweenTxBytes ;
extern volatile int rs_dir_interval;
extern int WaitForClientTimeOut;
extern volatile uint8_t TCP_STAT ;

void ldrun();

void AnalogAndOutputTrd()
{



	 AdcProc();
	 SpiUpdate();

}


extern char RunLeder ;
int main(void)


{
	//char RunScript = false;
#ifdef MBDEBUG
	char pbuf[50]="welcom to trend bug";
#endif


for(;;)
{

  WDTCTL = WDTPW + WDTHOLD;             // Stop watchdog timer
  //P1OUT |= BIT4; P1DIR |= BIT4;
  
  int OutPutTrdCnt =0;


  hardware_init();

	
  //UartSetup(0,9600,parity,OddParity,7,OneStopBit);	// trand port

  UartSetup(0,9600,NoParity,NoParity,8,OneStopBit);	// use  trand port for ethernet


 // if(ConfigLowBoudRate)
	  UartSetup(1,9600,NoParity,NoParity,8,OneStopBit);
  //else
	//  LoadUartConfig();//UartSetup(1,19200,NoParity,NoParity,8,OneStopBit);

	//V_SHABAT(true);
	//V_PULLUPS(true);
#ifdef MBDEBUG
 sprintf(&pbuf[0],"Welcome NOW to ternd ABC %d",RunLeder);
trendprintf(&pbuf[0]);
#endif

rs_dir_interval  =	read_eeprom_word(2005);

if(rs_dir_interval > 25 || rs_dir_interval < 0)
{
	write_eeprom_word(2005,1);
	rs_dir_interval = 1;
}

WaitForClientTimeOut =	read_eeprom_word(2007);

	if(WaitForClientTimeOut < 10 || WaitForClientTimeOut > 2000)
	{
		write_eeprom_word(2007,200);
		WaitForClientTimeOut = 200;
	}

	UartDelayBtweenTxBytes = read_eeprom_word(2009);
	if(UartDelayBtweenTxBytes > 0x40  )
	{
		write_eeprom_word(2009,4);
		UartDelayBtweenTxBytes = 4;

	}


if(CfgProgMaode)
	RunLeder = 0;
else
	RunLeder = CheckLdScr();// LoadLdScript();
	// LoadAnalogThVals(); // Load analog th value from add 2500  hith 2501 lowth
  for (;;)
  {
  	RSTWD;
#ifdef ETHERNET
  	//EthernetProc();


  	if(OneSecTimer == 0) // call every 10 msec TIMER RES 5ms
  	{
  		OneSecTimer = 10;// 50ms
  		P6OUT &= ~BIT2; // power on
  		P6OUT |= BIT0;// reset off
  		P6OUT |= BIT1;// cfg pin off

  		P3SEL |= BIT3;




  		if(TCP_STAT == STAT_IDEL)
  		{
  			if(TcpRstCnt >  32000)   // reset every 6 min
  			{
  				P6OUT |= BIT2;  // power off
  				P6OUT &= ~BIT0;// reset on
  				P6OUT &= ~BIT1;// cfg pin on

  				P3SEL &= ~BIT3; //mcu uart tx
  				P3OUT &= ~BIT3;
  			//	P3DIR |= BIT3;


  				OneSecTimer = 800;//4 sec 800 x 5ms
  				TcpRstCnt=0;
  				break;
  			}
  			else
  				TcpRstCnt++;
  		}else
  		{
  			TcpRstCnt =0;
  		}

  	}
	ModBusTcpProc();
#else
  	if(OneSecTimer == 0) // call every 10 msec
  	  	{
  	  		OneSecTimer = 10;// 10ms
  	//	if(RunLeder == 1)
  	//		ldrun();
  	//	else
#ifdef MBDEBUG
  		if(MBReadOneHolding(1,0,2))
  		{

  			sprintf(&pbuf[0],"RD %d\r\n",HoldingReg[0] );
  			trendprintf(&pbuf[0]);

  		}

#else
#ifdef TRNDBG
  		TrendSendToOutpu(0, 22 , 1);
#else
  		if(RunLeder == 2)
  		    HoteloOneSecCall();
#endif
#endif


  	}
#endif // ETHERNET

  	if(OutPutTrdCnt > 800)
  	{
  	//	AnalogAndOutputTrd();
  		//SpiUpdate();
  		OutPutTrdCnt =0;
  		if( GetPgmSwitchStat())
  		  		break; // do software reset
  	}else OutPutTrdCnt++;
	// AdcProc();


  }
}
}
