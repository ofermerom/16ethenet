/*
 * Trend.h
 *
 *  Created on: Jun 12, 2014
 *      Author: ofer.m
 */

#ifndef TREND_H_
#define TREND_H_

  bool SendToTrenFloat(int Address, int RamAddress,float flt);
 bool TrendSendToOutpu(int Address, int RamAddress, int val);
 bool TrendSendToOutpuUI(int Address, int RamAddress,unsigned int val);
 bool TrendReadReal(int Address,int RamAddress, float *ReturnValue );
 uint8_t MBSetSingleCoil(uint8_t hostid,uint16_t RegAdd,uint8_t val);

#endif /* TREND_H_ */
