/*
 * hardware.c
 *
 *  Created on: 24 ���� 2012
 *      Author: ofer.m
 */
#include "hardware.h"
#include "globals.h"
#include "eeprom.h"



volatile int  ethernetconnectiontimeout=0;
volatile unsigned char portbuff[50],portptr,InputsHReg=0,InverLedReg=0,ConfigLowBoudRate =0,CfgProgMaode = 0,UartDelayBtweenTxBytes = 2,eusartRxBuffer[150],eusartRxHead=0,eusartRxCount=0,eusartRxTail=0;
int CardConfigReg =0;


void SetMuxAdd(char add)
{
	if(add & 0x1)P1OUT |= BIT0;else P1OUT &= ~BIT0; 
	if(add & 0x2)P1OUT |= BIT1;else P1OUT &= ~BIT1;
	if(add & 0x4)P1OUT |= BIT5;else P1OUT &= ~BIT5;
		
}


void SpiSend(uchar by)
{
 // uchar pos = 0x01;
  /*
  while(true)
  {
	  //SPI_MOSI((pos & by));
	  if(pos & by)
	  {
	  	P4OUT |= BIT1;
	  }else
	  {
	  	P4OUT &=~BIT1;
	  }
	  __delay_cycles(4);
	  SPI_CLK(ON);
	  SPI_CLK(OFF);
	  if(pos == 0x80)break;
	  pos <<=1;
  }*/
	
}

int ControlLast=0;

void SpiUpdate()
{
/*

	__bic_SR_register(GIE);

	SPI_LE(OFF);

	SpiSend(InverLedReg);
	SpiSend(RLYreg);

	if(ControlLast != RLYreg)
	{

		ControlLast = RLYreg;
	}


	P3OUT |= BIT2;
	__delay_cycles(50);
	P3OUT &=~BIT2;
	
	P1SEL &=~BIT4;
	P1DIR |= BIT4;
	P1OUT &=~BIT4;
	__bis_SR_register(GIE);*/
}


void SetLeds(uchar led,uchar OnOff)
{
	uchar pos = 0x1,invpos = 0x80;
	
	pos <<= led;
	invpos >>= led;
	if(OnOff)
	{
		LEDSreg |= pos ;
		InverLedReg |= invpos;
		//InputsReg |= pos ;	
	}else
	{
		LEDSreg &=~pos ;
		InverLedReg &=~invpos;
		//InputsReg &=~pos ;
	}
	coils[7-led + 8] = OnOff;
	
	
	SpiUpdate();
}


void SetInputHReg(uchar led,uchar OnOff)
{

	uchar pos = 0x1;
	pos <<= led;
	if(OnOff)
	{
		
		InputsHReg |= pos ;	
	}else
	{
		
		InputsHReg &=~pos ;
	}

	//coils[led + 16] = OnOff;
}


void SetRly(uchar rly,uchar OnOff)
{
/*
	uchar pos = 0x1;
  	pos <<= rly;
	if(OnOff)
	{
		RLYreg |= pos ; 
		P1SEL &=~BIT4; // cancel rly pwm output 
		P1OUT &=~BIT4;
		P1DIR |= BIT4;
		RLY_PWM_TM = 400;
			
	}else
	{
		RLYreg &=~pos ;
	}

	SpiUpdate();
	*/
}




void SetRlyOn(int rly)
{
/*
	//int i,j;
	TA0CCR1 = 510; //set relay pwm to max 

	P1SEL &=~BIT2; // cancel pwm output 
	P1OUT |= BIT2; // set pwm output to on	
	if(rly == 1)
	{
		P1OUT |= BIT4;

	}
	if(rly == 2)
	{
		P1OUT |= BIT5;
	}

	TA0CCR1 = 120;
   // P1SEL |= BIT2;
	RLY_PWM_TM = 400;*/
}





void UartSetup(char uart,unsigned int boud,char parityen,char PartyEven,char bits,char stopbits)
{
	
	// Fosc 12Mhz
	
	if(uart)
	{
		if(parityen)UCA1CTL0 |=UCPEN;
		if(PartyEven)UCA1CTL0 |=UCPAR;
		if(bits == 7)UCA1CTL0 |=UC7BIT;
		if(stopbits == 2)UCA1CTL0 |=UCSPB;
		
		UCA1MCTL &= ~(UCBRS_2 + UCBRS_4);
		switch(boud)
		{
			case 4800:	 UCA1BR0 = 0xc4; UCA1BR1 = 0x09; /* UCBRS1  */	break;
			case 9600:	 UCA1BR0 = 0xfc; UCA1BR1 = 0x04; /* UCBRS1  */	break;// was e2
			case 19200:	 UCA1BR0 = 0x71; UCA1BR1 = 0x02; /* UCBRS1  */	break;
			case 38400:	 UCA1BR0 = 0x38; UCA1BR1 = 0x01;   UCA1MCTL |= UCBRS_4;	break;
			case 57600:	 UCA1BR0 = 208; UCA1BR1 = 0x00;    UCA1MCTL |= UCBRS_2;	break;
			case 11520: UCA1BR0 = 104; UCA1BR0 = 0x00;    UCA1MCTL |= UCBRS_1;	break;
		}
		
		
		  	
	}else
	{
		if(parityen)UCA0CTL0 |=UCPEN;
		if(PartyEven)UCA0CTL0 |=UCPAR;
		if(bits == 7)UCA0CTL0 |=UC7BIT;
		if(stopbits == 2)UCA0CTL0 |=UCSPB;
		
		
			switch( boud)
			{
				case 4800:	 UCA0BR0 = 0xc4; UCA0BR1 = 0x09; /* UCBRS1  */	break;
				case 9600:	 UCA0BR0 = 0xfc; UCA0BR1 = 0x04; /* UCBRS1  */	break;
				case 19200:	 UCA0BR0 = 0x71; UCA0BR1 = 0x02; /* UCBRS1  */	break;
				case 38400:	 UCA0BR0 = 0x38; UCA0BR1 = 0x01;   UCA0MCTL |= UCBRS_4;	break;
				case 57600:	 UCA0BR0 = 208; UCA0BR1 = 0x00;    UCA0MCTL |= UCBRS_2;	break;
				case 11520: UCA0BR0 = 104; UCA0BR0 = 0x00;    UCA0MCTL |= UCBRS_2;	break;
			}
			
			
	  UCA0CTL1 |= UCSWRST;                      // **Put state machine in reset**
	  UCA0CTL1 |= UCSSEL_2;                     // SMCLK
	 // UCA0BR0 =  0xe2;                              // 1MHz 115200 (see User's Guide)
	  //UCA0BR1 = 0x4;                              // 1MHz 115200
	//  UCA1MCTL |= UCBRS_1;// +*/ UCBRF_0;            // Modulation UCBRSx=1, UCBRFx=0
	  UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
	  UCA0IE |= UCRXIE;   
	}

}

#define UART_MB_BR_LOC 2020

void  LoadUartConfig()
{
	unsigned int boud=0;
	char parityen,PartyEven,stopbits;
	
 switch(read_eeprom_word(UART_MB_BR_LOC))
 {
 	case 1:boud = 4800;   break;
 	case 2:boud = 9600;   break;
 	case 3:boud = 19200;   break;
 	case 4:boud = 38400;   break;
 	case 5:boud = 57600;   break;
 	case 6:boud = 11520;   break;
 	default: boud = 9600; break;
 }

 switch(read_eeprom_word(UART_MB_BR_LOC+1))
  {
 	 case 0: parityen =0; PartyEven = 0;	 break;
 	 case 1: parityen =1; PartyEven = 0;	 	 break;
 	 case 2: parityen =1; PartyEven = 0;	 	 break;
 	 case 3: parityen =1; PartyEven = 1;		 break;
 	 default: parityen =0; PartyEven = 0;	 break;
  }

 switch(read_eeprom_word(UART_MB_BR_LOC+2))
   {
  	 case 1: stopbits = 1;	 	 break;
  	 case 2: stopbits = 2;	 	 break;
  	 default: stopbits = 1;	      break;
   }


 UartSetup(1, boud, parityen, PartyEven,8, stopbits);

}



void uart_init(void)
{
	  UCA1CTL1 |= UCSWRST;                      // **Put state machine in reset**
	  UCA1CTL1 |= UCSSEL_2;                     // SMCLK
	  UCA1BR0 =	0xe2 ;//9600	104;//115200 0xe2;                      // 1MHz 115200 (see User's Guide)
	  UCA1BR1 = 0x4;//0x4;                              // 1MHz 115200
	  
	  UCA1MCTL |= UCBRS_1 ;//+*/ UCBRF_0;            // Modulation UCBRSx=1, UCBRFx=0
	  UCA1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
	  UCA1IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
	  	  
	  	  
	  UCA0CTL1 |= UCSWRST;                      // **Put state machine in reset**
	  UCA0CTL1 |= UCSSEL_2;                     // SMCLK
	  UCA0BR0 =  0xe2;                              // 1MHz 115200 (see User's Guide)
	  UCA0BR1 = 0x4;                              // 1MHz 115200
	  UCA1MCTL |= UCBRS_1;// +*/ UCBRF_0;            // Modulation UCBRSx=1, UCBRFx=0
	  UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
	  UCA0IE |= UCRXIE;   
	  	  
}



void putcx(uchar ch,uchar uart)
{ 
	if(uart == UART2)
	{
	// while (!(UCA1IFG&UCTXIFG));             // USCI_A1 TX buffer ready?
	 UCA1TXBUF = ch;
	 UCA1IFG &=~UCTXIFG;
	 while (!(UCA1IFG & UCTXIFG));
	}
	if(uart == UART1)
	{
	// while (!(UCA0IFG&UCTXIFG));             // USCI_A1 TX buffer ready?
	 UCA0TXBUF = ch;
	 UCA0IFG &=~UCTXIFG;
	 while (!(UCA0IFG & UCTXIFG));             // USCI_A1 TX buffer ready?
	}
	
	for( ch= 0 ;ch < UartDelayBtweenTxBytes;ch++ )
											uart = ch;

}

//void Timer_setup();
void usc_setup(void);



void AdcInit()
{

	   ADC10CTL0 |= ADC10SHT_2 + ADC10ON;        // ADC10ON, S&H=16 ADC clks
	   ADC10CTL1 |= ADC10SHP;                    // ADCCLK = MODOSC; sampling timer
	   ADC10CTL2 |= ADC10RES;                    // 10-bit conversion results
	   ADC10MCTL0 |= ADC10INCH_2;                // A0 ADC input select; Vref=AVCC
	   ADC10IE |= ADC10IE0;                      // Enable ADC conv complete interrupt

}

void TimerAinit(void)
{
	  P1SEL |=  BIT2;                       // P1.2 and P1.3 options select
	  TA0CCR0 = 512-1;                          // PWM Period
	  TA0CCTL1 = OUTMOD_7;                       // CCR1 reset/set
	  TA0CCTL2 = OUTMOD_7;                       // CCR1 reset/set
	  TA0CCTL3 = OUTMOD_7;                       // CCR1 reset/set
	  
	  TA0CCR1 = 256;                            // CCR1 PWM duty cycle
	  TA0CCR2 = 256;                            // CCR1 PWM duty cycle
	  TA0CCR3 = 350;                            // CCR1 PWM duty cycle
	  TA0CTL = TASSEL_2 + MC_1 + TACLR;         // SMCLK, up mode, clear TAR	
}

void TimerB_setup()
{
	  TA2CCTL0 = CCIE;                          // CCR0 interrupt enabled
	  TA2CCR0 = 50000;							// generate IRQ every 0.005s	
	  TA2CTL = TASSEL_2 + MC_2 + TACLR;         // SMCLK, c	  
}


char GetPgmSwitchStat()
{

	static unsigned char lastswstat=0;
	char result =0;
	unsigned char	tmp = P2IN;

	if(tmp !=  lastswstat)result =1;
		lastswstat = tmp;
	return result;
}


void GetCardAdd()
{
	

	int  Tcardaddres =0,pos=0x1,npos = 0x80,tmp;



		P2DIR = 0;P2SEL =0;
		Tcardaddres = 0;//P2IN;
		tmp = P2IN;
		while(true)
		{
			if(tmp & pos)
			//	Tcardaddres &= ~npos;
		//	else
				Tcardaddres |= npos;
			if(pos == 0x80)break;
			pos <<= 1;
			npos >>=1;
		}


		CardConfigReg = Tcardaddres;
		CfgInpus16 = 1;//		(Tcardaddres &  0x20) ? 1 : 0;
		ConfigLowBoudRate = (Tcardaddres &  0x80) ? 1 : 0;

		CfgProgMaode = (Tcardaddres &  0x40) ? 1 : 0;

		 Tcardaddres &= 0x3f;
		 CfgInpus16 = 1;
		 cardaddres = Tcardaddres & 0x3f;





	/*

	unsigned char  Tcardaddres =0;
	
	 
	 
	P2DIR = 0;P2SEL =0;
	Tcardaddres = ~P2IN;
	//Tcardaddres =~ Tcardaddres ;
	 
	 CardConfigReg = Tcardaddres;
	 CfgInpus16 = 1;//		(Tcardaddres &  0x20) ? 1 : 0;
	 ConfigLowBoudRate = (Tcardaddres &  0x80) ? 1 : 0;
	 //AnalogEnable = (Tcardaddres &  0x40) ? 1 : 0;

	 Tcardaddres &= 0x7f;

	CfgInpus16 = 1;
	 
	  cardaddres = Tcardaddres;
	// cardaddres = 0x1;*/
}


void hardware_init(void)
{
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	
	P1_INIT;
	P2_INIT;
	P3_INIT;
	P4_INIT;
	P5_INIT;
	P6_INIT;

	// AdcInit();

	 //ACTIVE_VSHABAT;
	 usc_setup();
	 TimerB_setup();

	 uart_init();
	
	 TimerAinit();

	GetCardAdd();
	P5OUT &= ~BIT2;// LED D2
	P4OUT &= ~BIT7;//LED D3
	uartEnableTx(0);
/*
	 cardaddres = 0;
	 
	 
	 int Cpol=1,i;
	 
	 for( i= 0;i<8;i++)
	 {
	 	SetMuxAdd(i);
	 	DelayMs(1);	 	
	 	cardaddres |= (P1IN & BIT6) ? Cpol : 0;
	 	Cpol *=2;	 	
	 }
	 
	 CfgInpus16 = (cardaddres & 0x80) ? 1 : 0;
	 cardaddres &= 0x7f;*/
	/*
	  cardaddres |= (P1IN & BIT0) ? BIT0 :0  ;
	  cardaddres |= (P1IN & BIT1) ? BIT1 :0  ;
	  cardaddres |= (P1IN & BIT5) ? BIT2 :0  ;
	  cardaddres |= (P1IN & BIT6) ? BIT3 :0  ;
	  cardaddres |= (P1IN & BIT7) ? BIT4 :0  ;
	  cardaddres |= (P2IN & BIT1) ? BIT5 :0  ;
	  cardaddres |= (P2IN & BIT2) ? BIT6 :0  ;	
	  cardaddres |= (P2IN & BIT3) ? BIT7 :0  ;
		*/	
	  
	  V_SHABAT(ON);
	  V_PULLUPS(ON);
	  __no_operation();                         // For debugger


	  __bis_SR_register(GIE);       //  interrupts enabled
}


void SetRlyOff(int rly)
{
	/*
	if(rly == 1)
	{
		P1OUT &= ~BIT4;
	}
	if(rly == 2) 
	{
		P1OUT &= ~BIT5;
	} */
	
}






// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
  switch(__even_in_range(ADC10IV,12))
  {
    case  0: break;                          // No interrupt
    case  2: break;                          // conversion result overflow
    case  4: break;                          // conversion time overflow
    case  6: break;                          // ADC10HI
    case  8: break;                          // ADC10LO
    case 10: break;                          // ADC10IN
    case 12:
    	adc_mem = ADC10MEM0;


    default: break;
  }
}

void DelayMs(int delay)
{
	while(delay)
	{
		__delay_cycles(12000);
		delay--;
	}
}

void delay(unsigned int times)
{
	while(times)
	{
		__delay_cycles(100);
		times--;
	}
} 

void uartEnableTx(char st)
{
	if(st)
	{
		P4OUT |= BIT3; 
		P6OUT |=BIT7;
		__delay_cycles(2000);	
	}
	else
	{
		//__delay_cycles(10000);
		 P4OUT &=~BIT3;
		P6OUT &=~BIT7;
	}
	
}



char bytecount =0,readptr =0; 
unsigned char PortReadByte(void)
{		
	unsigned char result;
	if(bytecount)
	{
		result = portbuff[readptr++];
		if(readptr == 48)readptr=0;
		bytecount--;
	}
	
	return result; 
}

void PortSendByte(unsigned char ch)
{
	while (!(UCA0IFG&UCTXIFG));             // USCI_A1 TX buffer ready?	 
	UCA0TXBUF = ch;
}


void trendprintf(char *buff)
{

	while(*buff)
		PortSendByte(*buff++);
}



char BytesToRead()
{
	return bytecount;
}



volatile  uint8_t eusartRxBuffer2[20],eusartRxHead2 =0,eusartRxCount2=0,eusartRxTail2=0;

uint8_t EUSART_Read2(void) {
    uint8_t readValue = 0;

    while (0 == eusartRxCount2) {
    }


    readValue = eusartRxBuffer2[eusartRxTail2++];
    if (sizeof (eusartRxBuffer2) <= eusartRxTail2) {
        eusartRxTail2 = 0;
    }
    eusartRxCount2--;


    return readValue;
}




uint8_t EUSART_Read(void) {
    uint8_t readValue = 0;

    while (0 == eusartRxCount) {
    }

    //PIE1bits.RCIE = 0;

    readValue = eusartRxBuffer[eusartRxTail++];
    if (sizeof (eusartRxBuffer) <= eusartRxTail) {
        eusartRxTail = 0;
    }
    eusartRxCount--;
   // PIE1bits.RCIE = 1;

    return readValue;
}

volatile char tflg =0;
int ptik =0;









void EthernetProc()
{
#ifdef ETHERNET
if(ptik)ptik--;
if(eusartRxCount)
{
	if(tflg==0)
		uartEnableTx(TRUE);
 if(ptik ==0 )
 {
	putcx(EUSART_Read(),UART2);
	ptik = 300;
	tflg = 1;
 }
 ethernetconnectiontimeout = 180;

}else
{
	if(tflg==1 && ptik == 0)
	{
		uartEnableTx(FALSE);
	    tflg = 0;
	}
}

if(eusartRxCount2)
{
	putcx(EUSART_Read2(),UART1);
}


if(ethernetconnectiontimeout==0) // reset on timeout
{
  P6OUT &=~BIT0;
  ethernetconnectiontimeout = 2;
  while(ethernetconnectiontimeout);
  ethernetconnectiontimeout = 180;
  P6OUT |= BIT0;
}



#endif
}


extern volatile char SlaveRespons,EnableTcpRec  ;
extern volatile uint8_t TCP_STAT,TcpRecTimer ;
char rtm;
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
{
#ifdef ETHERNET

	if(EnableTcpRec)
	{
	  eusartRxBuffer[eusartRxCount++] = UCA0RXBUF;
	  TcpRecTimer = 0;
	  if(TCP_STAT == 0)  TCP_STAT = 1;// stat rec data
	}else
		rtm =  UCA0RXBUF;
#else

	portbuff[portptr++] = 	UCA0RXBUF;	
	if(portptr == 48)portptr=0;
	bytecount++;
#endif
}



// Echo back RXed character, confirm TX buffer is ready first
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
{
#ifdef ETHERNET_

	eusartRxBuffer2[eusartRxHead2++] = UCA1RXBUF;
		    if (sizeof (eusartRxBuffer2) <= eusartRxHead2)
		    {
		        eusartRxHead2 = 0;
		    }
		    eusartRxCount2++;


	//putcx(UCA1RXBUF,UART1);

#else
	MBUartRecData( UCA1RXBUF);

#endif
}






void SetVcoreUp(unsigned int level)
{
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}






void usc_setup(void)
{
  volatile unsigned int i;

  WDTCTL = WDTPW+WDTHOLD;                   // Stop WDT

  UCSCTL3 |= SELREF_2;                      // Set DCO FLL reference = REFO
  UCSCTL4 |= SELA_2;                        // Set ACLK = REFO
  // Increase Vcore setting to level1 to support fsystem=12MHz
  // NOTE: Change core voltage one level at a time..
  SetVcoreUp (0x01);
  // Initialize DCO to 12MHz
  __bis_SR_register(SCG0);                  // Disable the FLL control loop
  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
  UCSCTL1 = DCORSEL_5;                      // Select DCO range 24MHz operation
  UCSCTL2 = FLLD_1 + 374;                   // Set DCO Multiplier for 12MHz
                                            // (N + 1) * FLLRef = Fdco
                                            // (374 + 1) * 32768 = 12MHz
                                            // Set FLL Div = fDCOCLK/2
  __bic_SR_register(SCG0);                  // Enable the FLL control loop

  // Worst-case settling time for the DCO when the DCO range bits have been
  // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
  // UG for optimization.
  // 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
  __delay_cycles(375000);

  // Loop until XT1,XT2 & DCO fault flag is cleared
  do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag

 // while(1)
  //{
    //P1OUT ^= BIT1;                          // Toggle P1.0
    __delay_cycles(600000);                 // Delay
  //}
}


int ledtimer[4]={4,4,4};

volatile int ping=0;
#pragma vector=TIMER2_A0_VECTOR
__interrupt void TIMER2_A0_ISR(void)
{
	//if(ptik)ptik--;
// irq every 5ms
//char i=1;



 if(ping > 100){

	 if(ethernetconnectiontimeout)ethernetconnectiontimeout--;
 	//for(i=2;i<3;i++)
 	//	if(ledtimer[i])ledtimer[i]--; else Led(i,OFF);
	//	 P5OUT ^= BIT3;
	BlikLed3;
	 ping =0;
 }else ping++;

 if(RLY_PWM_TM)RLY_PWM_TM--;//else P1SEL |= BIT4;// active PWM on port
 if(OneSecTimer)OneSecTimer--;	
  MBtimerCall();
  TA2CCR0 += 60000;  // 0.005 irq                       // Add Offset to CCR0
}



void Led(char led,char on)
{
	switch(led)
	{
		case 1: if(on)P5OUT &=~BIT3; else{P5OUT |= BIT3;ledtimer[1] = 10; }   break;
		case 2: if(on) P5OUT &=~BIT2;else{P5OUT |= BIT2;ledtimer[2] = 10; }    break;
		case 3: if(on) P4OUT &=~BIT7;else{P4OUT |= BIT7;ledtimer[2] = 10; }  break;
	}

}







#pragma vector=DMA_VECTOR
__interrupt void DMA0_ISR (void)
{
//	if(ADC_Result[0] >INPUT_TH)P6OUT &=~LED1;else P6OUT |= LED1;
//char i=0;
	//for(i=0;i< 8;i++)
 	 // SetLeds(0, ADC_Result[0] >INPUT_TH ? OFF: ON);

  //  	if(ADC_Result[1] >INPUT_TH)P6OUT &=~LED2;else P6OUT |= LED2;
   // 	if(ADC_Result[2] >INPUT_TH)P6OUT &=~LED3;else P6OUT |= LED3;

  switch(__even_in_range(DMAIV,16))
  {
    case  0: break;                          // No interrupt
    case  2:
      // sequence of conversions complete
      //__bic_SR_register_on_exit(CPUOFF);     // exit LPM
      break;                                 // DMA0IFG
    case  4: break;                          // DMA1IFG
    case  6: break;                          // DMA2IFG
    case  8: break;                          // Reserved
    case 10: break;                          // Reserved
    case 12: break;                          // Reserved
    case 14: break;                          // Reserved
    case 16: break;                          // Reserved
    default: break;
  }
}



void AdcProc(void)
{
#ifdef DI16
	 int tmp = P6IN,pos = 0x1,npos=0x80;

		InputsHReg = ~tmp;//  P6IN;

		LEDSreg =InputsHReg ;
		InverLedReg =InputsHReg;

		tmp = P1IN;

	int	ReadRlyreg = 0;
		while(true)
		{
			if((pos & tmp)==0)ReadRlyreg |= npos;
			if(pos == 0x80)break;
			pos <<=1;
			npos >>=1;
		}

		RLYreg	= ReadRlyreg ;






#else

 static char muxsel =0;
 	REFCTL0 = REFON+REFMSTR;
 
	//__delay_cycles(5000);
	__delay_cycles(1000);
	while (ADC10CTL1 & ADC10BUSY);          // Wait if ADC10 core is active
	ADC10CTL0 = 0;
	ADC10MCTL0 =ADC10SREF_3;//ADC10SREF_1
	ADC10CTL0 |= ADC10SHT_2 + ADC10ON;        // ADC10ON, S&H=16 ADC clks
		   ADC10CTL1 |= ADC10SHP;                    // ADCCLK = MODOSC; sampling timer
		   ADC10CTL2 |= ADC10RES;
	
	 




	switch(active_adc_ch)
    	{					
			case 0:ADC_Result[0] = adc_mem;active_adc_ch =1; ADC10MCTL0 |= ADC10INCH_1;  break;
	    	case 1:ADC_Result[1] = adc_mem;active_adc_ch =2; ADC10MCTL0 |= ADC10INCH_2;  break;
	    	case 2:ADC_Result[2] = adc_mem;active_adc_ch =3; ADC10MCTL0 |= ADC10INCH_3;  break;
	    	case 3:ADC_Result[3] = adc_mem;active_adc_ch =4; ADC10MCTL0 |= ADC10INCH_4;  break;
	    	case 4:ADC_Result[4] = adc_mem;active_adc_ch =5; ADC10MCTL0 |= ADC10INCH_5;  break;
	    	case 5:ADC_Result[5] = adc_mem;active_adc_ch =6; ADC10MCTL0 |= ADC10INCH_6;  break;
	    	case 6:ADC_Result[6] = adc_mem;active_adc_ch =7; ADC10MCTL0 |= ADC10INCH_7;  break;
	    	case 7:ADC_Result[7] = adc_mem;active_adc_ch =8; ADC10MCTL0 |= ADC10INCH_8;  break;
	    	case 8:ADC_Result[8] = adc_mem;active_adc_ch =9; ADC10MCTL0 |= ADC10INCH_9;  break;
	    	
	    	case 9:
	    				ADC_Result[9 + muxsel] = adc_mem;
	    				SetInputHReg(muxsel,ADC_Result[9 + muxsel] >INPUT_TH ? OFF: ON);
	    				
	    				if(CfgInpus16)
	    					SetRly(muxsel,ADC_Result[active_adc_ch+muxsel] > INPUT_TH ? OFF: ON);
	    				
	    				muxsel++;
	    				if(muxsel > 0x7)muxsel=0;
	    				SetMuxAdd(muxsel);
	    				active_adc_ch =0;
	    				ADC10MCTL0 |= ADC10INCH_0 ;
	    				 
	    				  break;
    	}
    	if(active_adc_ch < 8)
    		SetLeds(active_adc_ch, ADC_Result[active_adc_ch] >INPUT_TH ? OFF: ON);
    		
    //	if(active_adc_ch == 9 && CfgInpus16)	
    	//		SetRly(muxsel,ADC_Result[active_adc_ch+muxsel] > INPUT_TH ? OFF: ON);

	ADC10CTL0 |= ADC10ENC + ADC10SC;

#endif
}

