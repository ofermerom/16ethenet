################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5310.cmd 

C_SRCS += \
../Eeprom.c \
../Ethernetmain.c \
../Hardware.c \
../I2cdrv.c \
../LdProc.c \
../TrendProtocol.c \
../mbcrc.c \
../modbus.c \
../modbustrend.c 

OBJS += \
./Eeprom.obj \
./Ethernetmain.obj \
./Hardware.obj \
./I2cdrv.obj \
./LdProc.obj \
./TrendProtocol.obj \
./mbcrc.obj \
./modbus.obj \
./modbustrend.obj 

C_DEPS += \
./Eeprom.pp \
./Ethernetmain.pp \
./Hardware.pp \
./I2cdrv.pp \
./LdProc.pp \
./TrendProtocol.pp \
./mbcrc.pp \
./modbus.pp \
./modbustrend.pp 

C_DEPS__QUOTED += \
"Eeprom.pp" \
"Ethernetmain.pp" \
"Hardware.pp" \
"I2cdrv.pp" \
"LdProc.pp" \
"TrendProtocol.pp" \
"mbcrc.pp" \
"modbus.pp" \
"modbustrend.pp" 

OBJS__QUOTED += \
"Eeprom.obj" \
"Ethernetmain.obj" \
"Hardware.obj" \
"I2cdrv.obj" \
"LdProc.obj" \
"TrendProtocol.obj" \
"mbcrc.obj" \
"modbus.obj" \
"modbustrend.obj" 

C_SRCS__QUOTED += \
"../Eeprom.c" \
"../Ethernetmain.c" \
"../Hardware.c" \
"../I2cdrv.c" \
"../LdProc.c" \
"../TrendProtocol.c" \
"../mbcrc.c" \
"../modbus.c" \
"../modbustrend.c" 


