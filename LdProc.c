
#include "eeprom.h"


#define uint8_t unsigned char
#define uint16_t unsigned int
#define false 0
#define true 1

#define bool char
#define uint unsigned int
#define uchar unsigned char



uint8_t GetCoilStat(uint8_t coil);
void  SetCoilStat(uint16_t coil,uint16_t val);
uint8_t GetCoilStat(uint8_t coil);

char Blink=0,BlCnt =0;

#define MAXMEM 300
#define CMDMASK 0xf000
#define VALUE_MASK 0xfff



#define LDDIFF_UP 0x1800
#define LDNOTDIFF_UP 0x2800
#define ORDIF_UP 0x3800
#define ORNOTDIFF_UP 0x4800
#define ANDDIFF_UP 0x5800
#define ANDNOTDIFF_UP 0x6800


#define LDDIFF_DOWN 0x7800
#define LDNOTDIFF_DOWN 0x8800
#define ORDIF_DOWN 0x9800
#define ORNOTDIFF_DOWN 0xA800
#define ANDDIFF_DOWN 0xB800
#define ANDNOTDIFF_DOWN 0xC800
#define KEEP 0xf000




#define NOP 0x0000
#define LD 0x1000
#define LDNOT 0x2000
#define OR 0x3000
#define ORNOT 0x4000
#define AND 0x5000
#define ANDNOT 0x6000
#define START 0x7000
#define OUT 0x8000
#define OUTNOT 0x9000
#define ANDLD 0xa000
#define TIM 0xb000
#define ORLD 0xd000
#define END  0xc000



#define INPUTSREG 32
#define TRSREGS   48
#define TIMERSREG 64
#define OUTPUTSREGS 100
#define GPREGS 200

#define CMDMASK 0xf000
#define REGSMASK 0xfff




#define MAX_SCR_LEN 600



		uint commands[400];
        bool TRs[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bool Timers[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        uint TimerCnts[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bool GpRegs[50]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        bool LoadCmds =true;


        uint CommandMode = 0;


        void LoadCommandRam()
        		{
        			uint CmdPtr=1;
        			for(CmdPtr = 0;CmdPtr < MAXMEM ; CmdPtr++)
        			{
        				if((commands[CmdPtr] = read_eeprom_word(CmdPtr)) == END)break;
        			}

        		}

#define LD_MODE 10
#define ModbusTRD_MODE 21


        bool CheckLdScr()
              {
                  uint checksum =0,Dread;
                  int cnt=0;
                  bool result = 0;

                  CommandMode = read_eeprom_word(2000); // was 1000


                  if(CommandMode == LD_MODE)
                  {

					  while(cnt < MAX_SCR_LEN)
					  {
						  Dread = read_eeprom_word(cnt);
						  commands[cnt] = Dread;
								   cnt++;
						  checksum += Dread;
						  if(Dread==END)
						  {
							  if(checksum == read_eeprom_word(cnt))
								  result = true;
							  else
								  CommandMode =0;
							  break;
						  }

					  }

					  if(result)LoadCommandRam();
					  result =1;
                  }
                  else if(CommandMode==ModbusTRD_MODE)
                  {

                	  while(cnt < MAX_SCR_LEN)
                	  {

                		  Dread = read_eeprom_word(cnt);
                		  cnt++;
                		  checksum += Dread;

                		  checksum += read_eeprom_word(cnt);
                		  cnt++;

                		  checksum += read_eeprom_word(cnt);
                		  cnt++;


                		  checksum += read_eeprom_word(cnt);//add for report fail
                		  cnt++;



                		  checksum += read_eeprom_word(cnt);//add for amount to read and offset
                		  cnt++;


                		  Dread &=0xf000;
                		   if(Dread==0x7000)
                		   {

                			   if(checksum == read_eeprom_word(cnt))
                			  								  result = true;
                			   //else
                				  // CommandMode =0;
                			   break;

                		   }
                	  }
                	  	  result = 2;

                  }
                 else
                	 result = false;

              //    result = 2;//****************************************
                  return result;
              }









    inline bool GetInput(int add)
        {
            return GetCoilStat((uchar)add);
        }

        bool GetTrs(int add)
        {
            return TRs[add];
        }

        bool GetTimer(int add)
        {
            return Timers[add];
        }

        inline bool GetOutPuts(int add)
        {
            return  GetCoilStat(add);	//		outputs[add];
        }

        bool GetBitReg(int add)
        {

            return GpRegs[add];
        }


        void SetTrs(uint add,bool val)
        {
            TRs[add]= val;
        }

        void SetOutPuts(uint add, bool val)
        {
        	 SetCoilStat(add,(uint)val);

        }

        void SetBitRegs(uint add, bool val)
        {
        	if(add == 50)LoadCmds = val;
        	if(add > 50)add = 0;
             GpRegs[add]=val;
        }



        bool CheckRegVal(int loc)
        {
            bool result =false;
            int add = commands[ loc] & 0xfff ;
            if (add < 32)
              result=  GetInput(add);
            else if(add < 48)
                result = GetTrs(add-32);
            else if (add < 99)
                result = GetTimer(add-64);
            else if (add > 99 && add < 200)
                result = GetOutPuts(add);
            else if (add > 199 && add < 250)
                result = GetBitReg(add - 200);
            else if(add == 250)
                       result = Blink;
                return result;
        }

        void SetReg(uint add, bool val)
        {

            if (add < 48)
                SetTrs(add - 32,val);
            else if (add > 99 && add < 200)
                SetOutPuts(add - 100,val);
            else if (add > 199 && add < 250)
            	SetBitRegs(add - 200, val);
        }


        void TimeCall(uint timer,bool input,uint loadval)
        {
            timer -= 64;
            if (input)
            {
                if (TimerCnts[timer] > 0)
                {
                    Timers[timer] = false;
                    TimerCnts[timer]--;
                }
                else
                {
                    Timers[timer] =true;
                }
            }
            else
            {
                TimerCnts[timer] = loadval;
                Timers[timer] = false;
            }

        }




        uint   ReadCmd( int loc)
        {
            return (uint)commands[loc] & 0xf000;
        }




        uint GetCmd(int loc)
        {
            return ((uint)commands[loc] & 0xf000);
        }

        uint GetPram(int loc)
        {
            return ((uint)commands[loc] & 0xfff);
        }
        uint GetCmdMemVal(int loc)
        {
            return ((uint)commands[loc] );
        }


        void ldrun()
        {


        	if(LoadCmds)
        	{
        		LoadCommandRam();
        		LoadCmds =false;
        	}
                int cmdptr=0;
                int lddip = 0;
                bool result[16];// true;

                if(BlCnt)BlCnt--;
                              else
                              {
                                  Blink = Blink ? 0:1;
                                  BlCnt = 10;
                              }

                while (GetCmd(cmdptr) != END)
                {
                    if (cmdptr == 14)
                    {

                    }

                    switch (GetCmd(cmdptr))
                    {

                        case LD:
                                 result[lddip] = CheckRegVal(cmdptr);
                                 cmdptr++;
                                 lddip++;
                                 break;
                        case LDNOT:
                                   result[lddip] = CheckRegVal(cmdptr) ? false : true;
                                   cmdptr++;
                                   lddip++;
                                   break;
                        case ANDLD:
                                   lddip--;
                                    result[lddip - 1] &= result[lddip];
                                    cmdptr++;
                                    break;
                        case ORLD:
                                    lddip--;
                                    result[lddip - 1] |= result[lddip];
                                    cmdptr++;
                                   // lddip--;
                                    break;


                        case AND: result[lddip - 1] &= CheckRegVal(cmdptr); cmdptr++; break;
                        case ANDNOT: result[lddip - 1] &= (CheckRegVal(cmdptr) ? false : true); cmdptr++; break;
                        case OR: result[lddip - 1] |= CheckRegVal(cmdptr); cmdptr++; break;
                        case ORNOT: result[lddip - 1] |= (CheckRegVal(cmdptr) ? false : true); cmdptr++; break;

                        case TIM: TimeCall(GetPram(cmdptr), result[lddip - 1], GetCmdMemVal(cmdptr + 1)); cmdptr += 2;lddip = 0; break;
                        case OUT: SetReg(GetPram(cmdptr), result[lddip - 1]);if(GetPram(cmdptr) >99) lddip = 0; cmdptr++; break;

                        case KEEP:	if(result[lddip - 1] != result[lddip - 2] ){if(result[lddip - 1]  ) SetReg(GetPram(cmdptr),false);	if(result[lddip - 2]  ) SetReg(GetPram(cmdptr),true);	}lddip--;		if(GetPram(cmdptr) >99) lddip = 0; cmdptr++; 						 break;
                        case OUTNOT: SetReg(GetPram(cmdptr), (result[lddip - 1] ? false : true)); if (GetPram(cmdptr) > 99) lddip = 0; cmdptr++; break;
                        case END: cmdptr = 0; lddip = 0; break;
                        default: cmdptr++; break;

                    }

                }



        }
