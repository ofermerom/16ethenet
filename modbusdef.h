/*
 * modbusdef.h
 *
 * 
 *      Author: ofer.m
 */

#ifndef MODBUSDEF_H_
#define MODBUSDEF_H_



#define MAX_SEND_RETRY  4

#define WAIT_HOST_REP_TO_INTERVAL 62









typedef enum
{
    STATE_RX_IDLE,
	WAIT_FOR_HOST_REP,             /*!< Receiver is in idle state. */
    STATE_RX_RCV,               /*!< Frame is beeing received. */
    STATE_RX_WAIT_EOF,           /*!< Wait for End of Frame. */
	STATE_RX_FARM_REC
} MBRcvState;



typedef enum
{
    STATE_TX_IDLE,              /*!< Transmitter is in idle state. */
    STATE_TX_START,             /*!< Starting transmission (':' sent). */
    STATE_TX_DATA,              /*!< Sending of data (Address, Data, LRC). */
    STATE_TX_END,               /*!< End of transmission. */
    STATE_TX_NOTIFY,             /*!< Notify sender that the frame has been sent. */
	STATE_TX_DAMYY
} MBSndState;


#define DAMY_BYTE 0x33



#define SLAVE_ADD_UVT 		0
#define SLAVE_ADD_FLOW 		1
#define SLAVE_ADD_DOSE 		2
#define SLAVE_ADD_POWER  	3
#define SLAVE_ADD_US_ON		4
#define SLAVE_ADD_PRESSURE	5
#define SLAVE_ADD_UV_WATS	6
#define SLAVE_ADD_STATUS1 	7
#define SLAVE_ADD_UVT1 		8
#define SLAVE_ADD_AGEMS1 	9
#define SLAVE_ADD_AGE1 		10
#define SLAVE_ADD_TEMP1		11
#define SLAVE_ADD_LAMPKW1 	12
#define SLAVE_ADD_LSM1		13
#define SLAVE_ADD_WTM1		14


#define SLAVE_ADD_STATUS2 	15
#define SLAVE_ADD_UVT2 		17
#define SLAVE_ADD_AGEMS2 	18
#define SLAVE_ADD_AGE2 		19
#define SLAVE_ADD_TEMP2		20
#define SLAVE_ADD_LAMPKW2 	21
#define SLAVE_ADD_LSM2		22
#define SLAVE_ADD_WTM2		23


#define SLAVE_ADD_SYS_ON	24
#define SLAVE_ADD_US_ENB	25
#define SLAVE_ADD_US_EVERY	26

#define SLAVE_ADD_US_EVERY	26
#define SLAVE_ADD_US_DURATION	27
#define SLAVE_ADD_DOSE_POWER	28
#define SLAVE_ADD_SYSTEM_POWER	29
#define SLAVE_ADD_DOSE_SET		30
#define SLAVE_ADD_MIN_DOSE		31
#define SLAVE_ADD_MIN_DOSE_AL	32
#define SLAVE_ADD_MAX_DOSE		33
#define SLAVE_ADD_LOW_FLOW_AL	34
#define SLAVE_ADD_REMOTE_ENB	35
#define SLAVE_ADD_REMOTE_POL	36
#define SLAVE_ADD_ALARM			37
#define SLAVE_ADD_FLOW_SW		38
#define SLAVE_ADD_FLOW_SW_POL	39
#define SLAVE_ADD_MAX_FLOW		40
#define SLAVE_ADD_PRESS_GAIN	41
#define SLAVE_ADD_PRESS_OFF		42
#define SLAVE_ADD_DOSE_AT_MIN	43
#define SLAVE_ADD_IGNITION		44

#define SLAVE_ADD_OVER_TEMP		45
#define SLAVE_ADD_LOW_STATUS	46

#define SLAVE_ADD_LSM_OFF		47
#define SLAVE_ADD_FLOW_AT_MIN	48
#define SLAVE_ADD_US_EXIST		49
#define SLAVE_ADD_MAL_STATUS	50

#define SLAVE_ADD_LM_TEMP1		51
#define SLAVE_ADD_LM_TEMP2		51




















#define MB_HOST_ID_POS 			0
#define MB_FUNCTION_CODE_POS	1
#define MB_BYTE_CONT_POS		2
#define MB_READ_COIL_CMD		0x01
#define MB_INPUTS_CMD			0x02
#define MB_READ_HOLDING_CMD		0x03
#define MB_READ_INPUTS_CMD      0x04
#define MB_WRITE_SINGLE_REG		0x06
#define MB_WRITE_SINGLE_COIL	0x05
#define MB_WRITE_MUL_COILS		0xf
#define MB_WRITE_MUL_REG		0x10


#define MB_FUNCTION_CODE_R_INPUT		0x03
#define MB_M_FUNCTION_CODE_W_HOLDING	0x10



#define  C_US_CLEAN_ADD	4


#define  H_UVT_ADD				0
#define  H_FLOW_ADD				1
#define  H_DOSE_ADD				2
#define  H_POWER_LAMP2_ADD		3
#define  H_PRESURE_ADD			5
#define  H_UV_POWER_ADD			6
#define  H_STATUS_LAMP1_ADD 	7
#define  H_UVT_LAMP1_ADD		8
#define  H_MSB_AGE_LAMP1_ADD	9
#define  H_LSB_AGE_LAMP1_ADD	10
#define  H_TEMP_LAMP1_ADD		11


#define MBM_INPUT_REGS_STADD		0
#define MBM_INPUT_REGS_COUNT		13

#define MBM_LAMP_STAT_INP_ADD		0
#define MBM_FLOW_SW_INP_ADD			1
#define MBM_LAMP_VOLT_INP_ADD		2
#define MBM_LAMP_CURRENT_INP_ADD	3
#define MBM_LAMP_POWER_INP_ADD		4
#define MBM_UVIS_INP_ADD			5
#define MBM_WTH_INP_ADD				6
#define MBM_FLOW_METER_INP_ADD		7
#define MBM_PRESUR_METER_INP_ADD	8
#define MBM_REACT_TEMP_INP_ADD		9
#define MBM_INT_TEMP_INP_ADD		10
#define MBM_TC_FALT					11
#define MBM_LM_CARD_SW_VER_INP_ADD  12



#define MBM_REGISTERS_COUNT			7
#define MBM_HOLD_REG_STADD			100
#define MBM_LAMP_CTL_WR_ADD			100
#define MBM_IN_VALVE_WR_ADD			101
#define MBM_OUT_VALVE_WR_ADD		102
#define MBM_BYPASS_CTL_WR_ADD		103
#define MBM_US_MUX_WR_ADD			104
#define MBM_US_RESERV_WR_ADD		105
#define MBM_LAMP_PWR_CTL_WR_ADD		106
#define MBM_MAX_LAMP_PWR_WR_ADD		200


#define LAMP_POWER_REG 				7

#define WRITE_UPPER_REG 			2
#define WRITE_LOWREGISTERS			1
#define READBACK_COUNT_POS  		0x5


#define HOST_GET_INTERVAL 			20


#define GetLByte(c) (c & 0xff)
#define GetHByte(c) ((c >> 8) & 0xff)





#endif /* MODBUSDEF_H_ */
