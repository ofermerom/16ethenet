
#include "hardware.h"
#include "globals.h"

extern UINT time_reg[8];

UINT send_byte(UINT dbyte)
{
 unsigned int tmp = 0x80;
// PULL_UP_ENB;
 CLR_SCL;
 while(1)
 {
  if(tmp & dbyte){SET_SDA;} else {CLR_SDA;}
  CLK_P;
  if(tmp == 0x1)break;
  tmp >>=1;
 }
  SDAINP;
  SET_SCL;
  tmp = (!SDA_VAL ?1:0);
  CLR_SCL;
  
  return tmp;
}


UINT WriteToEppromBuff(UINT ByteAdd,UINT *DataByte,UINT size)
{
  UINT SlaveAdd,i=0;
 
  SlaveAdd = 0xa0;
  if(ByteAdd & 0x200)SlaveAdd |= 0x4;
  if(ByteAdd & 0x100)SlaveAdd |= 0x2;

  
  START_IC;
  if(!send_byte(SlaveAdd))return false;
  if(!send_byte(ByteAdd))return false;

  while(size)
  {
   if(!send_byte(*DataByte))return false;
   size--;
   *DataByte++;
  }
  STOP_IC;

   while(true)
  {
   START_IC;
   if(send_byte(SlaveAdd))break;
   i++;
   STOP_IC;
   
  }

  return i;

}

UINT WriteToEpprom(UINT ByteAdd,UINT DataByte)
{
  UINT SlaveAdd,i=0;
  DataByte &= 0xff;
  
 
  SlaveAdd = 0xa0;
//  if(ByteAdd & 0x200)SlaveAdd |= 0x4;
//  if(ByteAdd & 0x100)SlaveAdd |= 0x2;


  START_IC;
  while(true){
     i=90;
  if(!send_byte(SlaveAdd))break;
  if(!send_byte(ByteAdd >> 8))break;
  if(!send_byte(ByteAdd))break;
  if(!send_byte(DataByte))break;
  i=0;
  break;
  }
  STOP_IC;

  while(true)
  {
   START_IC;
   if(send_byte(SlaveAdd))break;
   i++;
   STOP_IC;
  }
  STOP_IC;
  
  return i;
}

UINT read_byte(void)
{
 unsigned int tmp = 0x80,dread=0;

 CLR_SCL;
 SDAINP;
 while(1)
 {
  SET_SCL;
  if(SDA_VAL)dread |= tmp;
  CLR_SCL;
  if(tmp == 0x1)break;
  tmp >>=1;
 }
  SET_SCL;
  CLR_SCL;
   
  return dread;
}

UINT read_Epprom_byte(UINT eprom_add)
{

  UINT ed = 0xa0; // eeprom base address
 
 // if(eprom_add & 0x200)ed |= 0x4;
 // if(eprom_add & 0x100)ed |= 0x2;

  START_IC;
  UINT n=send_byte(ed);
  n=send_byte(eprom_add >> 8);
  n=send_byte(eprom_add);
  
  START_IC;
  send_byte(ed | 0x1 );
  ed = read_byte();
 
  STOP_IC;
  
  return ed+n;
}

#define  RTC_CODE  0x60




UINT rtc_wbyte(UINT dbyte)
{
  UINT tmp,i;
 
  tmp = 0x1;
  SDAOUT;
  CLR_SCL;
  while(1)
  {
    if(tmp & dbyte){SET_SDA;}else{ CLR_SDA;}
    CLK_P;
    if(tmp == 0x80)break;
    tmp <<=1;
  }
  SDAINP;
  SET_SCL;
  if(SDA_VAL)i=1;else i=0;
  CLR_SCL;
  
  return i;
}

UINT rtc_rdbyte(UINT sendack)
{
  UINT dread =0,tmp =0x1;
 
     CLR_SCL;
     SDAINP;
      while(1)
        {
          SET_SCL;
          if(SDA_VAL)dread |= tmp;
          CLR_SCL;
          if(tmp == 0x80)break;
          tmp <<=1;
        }
        SDAOUT; //send ack to rtc
        if(sendack){CLR_SDA;}else{ SET_SDA;}
        SET_SCL;
        CLR_SCL;
        SDAINP;
        
        return dread;
}












void START_IC_(void)
{
   //PULL_UP_ENB;
   CLR_SCL; SDAOUT; IBUSHI;CLR_SDA;CLR_SCL;
}

void STOP_IC_(void)
{
    SDAOUT; CLR_SDA;SET_SCL; SET_SDA ; CLR_SCL;RELEASE;SET_SCL;   //  CLR_SDA
}






