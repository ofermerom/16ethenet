/*
 * eeprom.h
 *
 *  Created on: 7 ���� 2013
 *      Author: ofer.m
 */

#ifndef EEPROM_H_
#define EEPROM_H_


unsigned int read_eeprom_word(unsigned int address);
char write_eeprom_word(unsigned int address,unsigned int data);
unsigned int read_eeprom_byte(unsigned int address);
char write_eeprom(unsigned int add,unsigned int data);

#endif /* EEPROM_H_ */
