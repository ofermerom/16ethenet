
#include "hardware.h"


#define uint unsigned int
#define byte unsigned char

extern uint commands[400];


#define EPWR_ADD 0xa0
#define EPRD_ADD 0xa1




void DelayUs(uint len)
{
 int i=2;
 while(i>0)i--;
}


void ClockPuls()
{
	CLR_SCL;
    DelayUs(6);
    SET_SCL;
    DelayUs(6);
    CLR_SCL;
    DelayUs(6);
}


void StartIC()
{
    SET_SDA ;
    SET_SCL;
    DelayUs(10) ;
    CLR_SDA ;
    DelayUs(10) ;
    CLR_SCL ;
}



unsigned char i2c_readbye(char rack)
{
	   byte pos = 0x80;
	    unsigned char result =0;
	    CLR_SCL;
	    SDAIN;
	    DelayUs(4);
	    while(true)
	    {
	       if(SDA_VAL)
	    	   	   result |= pos;
	        SET_SCL;
	        DelayUs(6);
	        CLR_SCL;
	        if(pos== 0x1)break;
	        pos >>=1;
	    }
	    if(rack)
	    {CLR_SDA;}// send ack
	    else
	    	SDAIN;// send ack
	      DelayUs(10);
	    SET_SCL;
	       DelayUs(4);
	    CLR_SCL;
	    SDAIN;
	     return result;

}





char i2c_sendbyte(uint data)
{
    uint pos = 0x80;
    while(1)
    {
        if(data & pos)
        {
        	SET_SDA;
        }
        else
        {
            CLR_SDA;
        }
        ClockPuls();
        if(pos== 0x1)break;
        pos >>=1;
    }
    SDAIN;
    SET_SCL;
     DelayUs(10);
    pos = SDA_VAL ? 0 : 1;
    CLR_SCL;
    DelayUs(12);
    return pos;
}



char  epStartReadSq(uint add)
{
		char result =true;
		StartIC();
		result &= i2c_sendbyte(EPWR_ADD);
		result &= i2c_sendbyte(add >> 8);
		i2c_sendbyte(add & 0xff);
		StartIC();
		result &= i2c_sendbyte(EPRD_ADD);
		return result;
}



/*
uint read_eeprom(uint add)
{
	 uint result=0;
	 if( epStartReadSq( add))
		result =readbyteic(true);
		STOP_IC;
	return result ;
}
*/

/*
byte i2c_eeprom_read_byte( int deviceaddress, unsigned int eeaddress )
{
  byte rdata = 0xFF;
  Wire.beginTransmission(deviceaddress);
  Wire.send((int)(eeaddress >> 8));    // Address High Byte
  Wire.send((int)(eeaddress & 0xFF));  // Address Low Byte
  Wire.endTransmission();
  Wire.requestFrom(deviceaddress,1);
  if (Wire.available()) rdata = Wire.receive();
  return rdata;
}
*/


unsigned int read_eeprom_byte(unsigned int address)
{
	unsigned int result =1;
	StartIC();
		result &= i2c_sendbyte(EPWR_ADD);
		result &= i2c_sendbyte(address >> 8);
		result &= i2c_sendbyte(address & 0xff);
		StartIC();
		result &= i2c_sendbyte(EPRD_ADD);
		result =i2c_readbye(false);

		STOP_IC;
		return result;
}






unsigned int read_eeprom_word(unsigned int address)
{
    unsigned int result=0;
    address *=2;

    result = read_eeprom_byte(address);
        	result <<=8;
        	result += read_eeprom_byte(address+1);
    /*
    if( epStartReadSq( address))
    {
    	result = i2c_readbye(true);
    	result <<=8;
    	result += i2c_readbye(true);
    }
    STOP_IC;*/
    return result;
}

/*
void i2c_eeprom_write_byte( int deviceaddress, unsigned int eeaddress, byte data )
{
  int rdata = data;
  Wire.beginTransmission(deviceaddress);
  Wire.send((int)(eeaddress >> 8));    // Address High Byte
  Wire.send((int)(eeaddress & 0xFF));  // Address Low Byte
  Wire.send(rdata);
  Wire.endTransmission();
}
*/




char write_eeprom(unsigned int add,unsigned int data)
{
	char result=true,ret =10;
	EEPROM_WP_DISABLE;
	ret = 160;
	while(ret)
	{
		result = true;
		StartIC();
		result &= i2c_sendbyte(EPWR_ADD);
		if(result)break;
		ret--;
		//STOP_IC;
	}
	if(ret==0)
	{
		ret=1;
	}

	result &=i2c_sendbyte((add & 0xff00 )>> 8);
	result &=i2c_sendbyte(add & 0xff);
	result &=i2c_sendbyte(data & 0xff);
	STOP_IC;
	return result;
}



char write_eeprom_word(unsigned int  address,unsigned int data)
{
   char result=true;
   address *=2;
   result &= write_eeprom(address,(data >>8)&0xff);
   result &= write_eeprom(address+1,(data & 0xff));
   return result;
}










