/*
 * Hardware.h
 *
 *  Created on: 24 ���� 2012
 *      Author: ofer.m
 */



#ifndef HARDWARE_H_
#define HARDWARE_H_


//#include <msp430.h>

#include <msp430f5310.h>

void hardware_init(void);
void Led(char led,char on);


#define MB_TX_ENB(c) if(c)P4OUT |= BIT3;else P4OUT &=~BIT3;
#define ON 1
#define OFF 0
//#define MUX_ENB(c) if(c) P2OUT &= ~BIT1; else P2OUT |= BIT1;
//#define SET_MUX_ADD(c)   if(c & BIT0)P2OUT |= BIT3;else P2OUT &=~BIT3;if(c & BIT1)P2OUT |= BIT4;else P2OUT &=~BIT4; if(c & BIT2)P1OUT |= BIT7;else P1OUT &= ~BIT7;
#define V_SHABAT(c) if(c)P2OUT |= BIT5;else P2OUT &=~BIT5;
#define V_PULLUPS(c) if(c)P4OUT |= BIT0;else P4OUT &=~BIT0;


#define BLINK_LED1 P5OUT ^= BIT3;
#define BLINK_LED2 P5OUT ^= BIT2;
#define LED1(c) if(c) P5OUT &=~BIT3; else P5OUT |=BIT3;
#define LED2(c) if(c) P5OUT &=~BIT2; else P5OUT |=BIT2;
#define LED3(c) if(c) P4OUT &=~BIT7; else P4OUT |=BIT7;

#define LED2ON  P5DIR |= BIT2; P5OUT &= ~BIT2
#define LED2OFF  P5DIR |= BIT2; P5OUT |= BIT2


#define SPI_MOSI(c) if(c) P4OUT |= BIT1 ; else P4OUT &=~BIT1; 
#define SPI_CLK(c) if(c) P2OUT |= BIT7 ; else P2OUT &=~BIT7; 
#define SPI_LE(c) if(c) P3OUT |= BIT2; else P2OUT &=~BIT2;



#define SPI_MISO (P4IN & BIT2)		

#define P1_INIT P1REN |= 0xff;P1OUT |= 0xff; P1DIR = 0;P1SEL =0;
#define P2_INIT P2REN |= 0xff ; P2OUT |= 0xff; P2DIR =0;P2SEL =0;
#define P3_INIT P3DIR |= BIT0 +  BIT2; P3SEL |= BIT3 + BIT4;P3OUT &= ~BIT0;

#define P4_INIT P4DIR |= BIT0 + BIT1 + BIT3 + BIT7 ; P4REN |= BIT2 ; P4OUT |= BIT2 ;P4SEL |= BIT4+ BIT5 

#define P5_INIT  P5DIR |= BIT2 + BIT3
#define P6_INIT P6SEL = 0;P6DIR = BIT0+BIT2 +BIT1+BIT7 ;P6OUT =0;   P6OUT |= BIT0+BIT1

#define ACTIVE_VSHABAT P2OUT |= BIT5 
#define SHABAT_MODE P2OUT &= ~BIT5

//#define  RLY_SET_PW P1SEL &=~BIT4; 	P1OUT &=~BIT4; P1DIR |= BIT4; 	RLY_PWM_TM = 400;

#define PxOUT P3OUT
#define PxIN P3IN
#define PxDIR P3DIR
#define SDA BIT6
#define SCL BIT0

#define SDAOUT P4DIR |= BIT6
//PxDIR |= SDA
#define SDAIN  P4DIR &=~ BIT6
//PxDIR &= ~SDA
#define IBUSHI P4DIR &=~ BIT6 ; P5DIR &=~BIT0
//PxOUT |= SDA | SCL

#define SDA_VAL (P4IN & BIT6 )
//(PxIN & SDA)
#define UINT unsigned int
#define UCHAR unsigned char
#define SET_SCL P5DIR |=SCL; P5OUT |= SCL
#define CLR_SCL P5DIR |=SCL; P5OUT &=~ SCL
#define SET_SDA P4DIR |=SDA;  P4OUT |= SDA;
#define CLR_SDA P4DIR |=SDA;  P4OUT &= ~SDA;
#define RELEASE  P4DIR &= ~ SDA
#define EEPROM_WP_DISABLE P5DIR |= BIT1; P5OUT &=~BIT1

void START_IC_(void);
void STOP_IC_(void);

#define START_IC  START_IC_()
#define STOP_IC  STOP_IC_()
#define SEND_BIT(c) if(c==1)SET_SDA; else CLR_SDA ;SET_SCL;CLR_SCL
#define CLK_P SET_SCL;CLR_SCL
#define SDAINP P4DIR &= ~ SDA




unsigned char PortReadByte(void);
char BytesToRead();
void PortSendByte(unsigned char dbyte);

#define UART1 0
#define UART2 1

void DelayMs(int delay);



#define BlinkLed1 P5OUT ^= BIT3
#define BlinkLed2 P5OUT ^= BIT2
#define BlikLed3 P4OUT ^= BIT7;



 #define DtrEnable P3OUT &= ~BIT0
 #define DtrDisable P3OUT |= BIT0
      

 #define stx 0x2
 #define etx 0x3
 #define eot 0x4
 #define enq 0x5
 #define ack 0x6
 
 
 
#define byte unsigned char 
#define bool char 
#define true 1
#define false 0
#define COM0 0
#define COM1 1
#define SerialPort char 

#define parity 1
#define NoParity 0
#define OddParity 0
#define EvenParity 1
#define OneStopBit 0

  bool TrendWriteReal(int Address, int RamAddress, byte RealByte1, byte RealByte2, byte RealByte3);
  bool TrendWriteByte(int Address, int RamAddress, int RamValue);
  bool PortSendBytes(byte *data, int len ,int expected);
  bool TrendReadByte(int Address,int RamAddress, unsigned int *ReturnValue );	

void UartSetup(char uart,unsigned int boud,char parityen,char PartyEven,char bits,char stopbits);
#endif /* HARDWARE_H_ */
