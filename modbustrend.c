

#include "hardware.h"
#include "globals.h"
#include "trend.h"

#include "eeprom.h"
#include "math.h"





#define TREND_ADD 23

#define MB_TO_TRD_HOLD 0x10
#define MB_TO_TRD_COIL 0x20
#define MB_TO_TRD_INPUT 0x30
#define TRD_TO_MB_HOLD 0x60
#define TRD_TO_MB_COIL 0x50
#define TRD_TO_MB_DBL 0x40
#define MB_TO_TRD_FLOAT 0x80
#define MB_TO_TRD_FLOAT_OP 0x90
#define  MB_TO_TRD_INPUTS_PINS  0xa0
#define TRD_TO_MB_FLOATE 0xb0
#define TRD_TO_MB_FLOATE_REV 0xc0
#define TRD_TO_MB_COIL_SIGNAL	0xd0
#define MB_TO_TRD_HOLD_TO_FLOATS	0xe0

#define MB_INPUT_TRD_FLOAT	0xf0
#define MB_INPUT_TRD_FLOAT_INV	0xf8
#define MB_TO_TRD_TWO_HOLD 0xf4

#define MB_TO_TRD_TWO_HOLD_INV 0x4



#define MB_INPUT_REG_TRD_INT  0x08

#define MB_FLOAT_TRD_FINT 0x18
#define MB_FLOAT_TRD_FINT_INV 0x28
#define MB_HOLD_TRD_TWO 0x38
#define TRD_TRIGER_NUM 0x48


#define RESERV1 0x58
#define RESERV2 0x68
#define RESERV3 0x78
#define RESERV4 0x88
#define RESERV5 0x98
#define RESERV6 0xa8



int trend_triger_not_to_send = 0x7ff0;
extern uchar cardaddres;
int vercount =0;

extern char	RunLeder ;

#define CHANGE_CHECK 0x4

//#define MB_INPUT_REG_TRD_INT  0xfc


#define EOF 0x70
                        //case "To Trend Real":  record[0] = 0x3000; break;
                        //case "To Modbus Real": record[0] = 0x4000; break;
                        //case "To Modbus Bit": record[0] = 0x5000; break;
                        //case "To Modbus Hold": record[0] = 0x6000; break;
                        //default: record[0] = 0x7000; break;// end of script


void Delay_n(int n)
	{
	int i=0,j=n,rn=0;
	while(j)
	{
		for(i=0;i< 100;i++)rn++;

			j--;
	}

	}




//Byte
// 0		Command + Priority
// 1		Modbus Card
// 2		TrendRegAdd Msb
// 3        TrendRegAdd Lsb
// 4		ModbusRegAdd Msb
// 5 		ModBusRegLsb

#define START_OF_RECORDS 0;


union
{
	unsigned int msb[2];
	//unsigned int lsb;
	unsigned long lng;
	unsigned char bytes[4];

	//float flt;

}MBlong;

union
{
	unsigned int mbuint;
	unsigned char bytes[2];

}Mbuint;


union
{
	long longr;
	unsigned int uints[2];

}Mbslong;



union
{
	unsigned int msb[2];
	//unsigned int lsb;
	unsigned long lng;
	float flt;

}MBfloat;

extern char ReadSucessFlage ;

unsigned char CardNotReplay[128];

bool TrendSendToOutpuErr(char  inerror, int ReportModbusFail, int Modbuscardadd)
{
	Modbuscardadd &= 0x7f;

	if(ReportModbusFail)
	{
		if(inerror)
		{
			if(CardNotReplay[Modbuscardadd] < 6)
				     CardNotReplay[Modbuscardadd]++;

			if(CardNotReplay[Modbuscardadd]  == 6)
			{
				TrendSendToOutpu(TREND_ADD, ReportModbusFail ,Modbuscardadd | 0x80);
				CardNotReplay[Modbuscardadd] = 7;
			}

		}else
		{
			if(CardNotReplay[Modbuscardadd] > 1 )
						CardNotReplay[Modbuscardadd]=1;
			else
			{
				if(CardNotReplay[Modbuscardadd]==1)
					if(TrendSendToOutpu(TREND_ADD, ReportModbusFail ,Modbuscardadd))
						CardNotReplay[Modbuscardadd]=0;



			}


		}

	}



	return true;//TrendSendToOutpu(TREND_ADD, RamAddress , val);
}


void HoteloOneSecCall()
{

	unsigned int  TrendRegNum=0,Modbusreg=2,ReportModbusFail=0;//ReadValue[3],
	unsigned char Modbuscardadd=0;
	unsigned char ReadAmount =1,ReadOffset=0;
	static unsigned int CmdPtr =START_OF_RECORDS;
	static unsigned int PrioretyLevel =0;

	int cmd =	read_eeprom_byte(CmdPtr++);

	int priority = cmd & 0x3,TrendNum;

//	int numx=0;
	Modbuscardadd = read_eeprom_byte(CmdPtr++);

	TrendRegNum = read_eeprom_byte(CmdPtr++);
	TrendRegNum *=0x100;
	TrendRegNum += read_eeprom_byte(CmdPtr++);

	Modbusreg = read_eeprom_byte(CmdPtr++);
	Modbusreg *=0x100;
	Modbusreg += read_eeprom_byte(CmdPtr++);

	ReportModbusFail = read_eeprom_byte(CmdPtr++);
	ReportModbusFail *=0x100;
	ReportModbusFail += read_eeprom_byte(CmdPtr++);


  	ReadAmount =read_eeprom_byte(CmdPtr++); // add new
  	ReadOffset =read_eeprom_byte(CmdPtr++);

  	if(ReadAmount > 30 )
  		ReadAmount = 1;
  	if(ReadOffset > 10)
  		ReadOffset = 0;

	//char TwoWayOp = (cmd & 0x4) ? true: false,DoTwoWay,i;
	//DoTwoWay = 0;
	char i;
	switch(cmd & 0xfc)
	{


		case TRD_TRIGER_NUM:
				trend_triger_not_to_send = TrendRegNum;
			break;


		case MB_TO_TRD_TWO_HOLD_INV:// 04 long numberr



					if(priority <= PrioretyLevel)
										{
											//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
											if(ReadAmount < 2)ReadAmount = 2;
											if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
											{

												Mbslong.uints[0] = HoldingReg[1+ReadOffset];//this is the msb
												Mbslong.uints[1] = HoldingReg[0+ReadOffset];


												MBfloat.flt = (float)Mbslong.longr;

												SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);

												TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

											}else
												if(ReportModbusFail)
														TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
										}


					break;





		case MB_TO_TRD_TWO_HOLD:// f4




/*
			if(MBReadOneHolding(Modbuscardadd,Modbusreg,2,READ_INPUTS_CMD))
								{
									TrendSendToOutpu(TREND_ADD, TrendRegNum , HoldingReg[0] );
									TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
								}else
									if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
*/




			if(priority <= PrioretyLevel)
								{
									//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
									if(ReadAmount < 2)ReadAmount = 2;
									if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
									{

										Mbslong.uints[0] = HoldingReg[0+ReadOffset];//this is the msb
										Mbslong.uints[1] = HoldingReg[1+ReadOffset];

										MBfloat.flt = (float)Mbslong.longr;

										SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);

										TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

									}else
										if(ReportModbusFail)
												TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
								}


			break;




		case MB_HOLD_TRD_TWO://0x38
			if(priority <= PrioretyLevel)
			{

				if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
				{
					Mbuint.mbuint = HoldingReg[0+ReadOffset];

					TrendSendToOutpu(TREND_ADD, TrendRegNum , Mbuint.bytes[0] );
					TrendSendToOutpu(TREND_ADD, TrendRegNum+1 , Mbuint.bytes[1] );
					TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}else
					TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);

			}

		break;



		case MB_TO_TRD_HOLD://0x10


			if(priority <= PrioretyLevel)
			{
				/*if(TwoWayOp)
				{
					if(TrendReadByte(TREND_ADD,TrendRegNum, &ReadValue[0]))
						DoTwoWay = ReadValue[0];
				}
				if(DoTwoWay)
				{
					if(TrendReadByte(TREND_ADD,TrendRegNum+1, &ReadValue[0]))

						if(MbSetRegisterValue(Modbuscardadd,Modbusreg,ReadValue[0]) == false && ReportModbusFail)
															TrendSendToOutpu(TREND_ADD, ReportModbusFail , 1);

				}else
				{*///Normal mode
					if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
						{
							TrendSendToOutpu(TREND_ADD, TrendRegNum , HoldingReg[ReadOffset] );
							TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
						}else
							if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
				}
			//}
			break;
		case MB_TO_TRD_COIL://0x20

				if(  MBReadCoilsInputs(Modbuscardadd,Modbusreg, 0))
				{
					HoldingReg[0] /= 0x100;
					MBfloat.flt = (float)HoldingReg[0];
					SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
					TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}
				else
					if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
			break;
		case MB_TO_TRD_INPUT://0x30

			if(cardaddres == Modbuscardadd && Modbusreg == 60)
			{

				if(vercount )vercount--;
				else
				{
					if(SendToTrenFloat(TREND_ADD, TrendRegNum,VERSION_))
						vercount = 500;
					else
						vercount = 4;
				}


			}else
			{

			if(  MBReadCoilsInputs(Modbuscardadd,Modbusreg, 1))
			{
						HoldingReg[0] /= 0x100;
						MBfloat.flt = (float)HoldingReg[0];
						SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
						TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
			}
			else
				if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
			}
			break;


		case MB_TO_TRD_FLOAT://0x80

			if(ReadAmount< 2)ReadAmount=2;
			if(priority <= PrioretyLevel)
						if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
						{
							MBfloat.msb[0] = HoldingReg[0+ReadOffset];
							MBfloat.msb[1] = HoldingReg[1+ReadOffset];

							SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
							TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

						}else
						if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);


			break;


		case MB_TO_TRD_FLOAT_OP://0x90

			if(ReadAmount< 2)ReadAmount=2;
					if(priority <= PrioretyLevel)
								if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
								{
									MBfloat.msb[0] = HoldingReg[1+ReadOffset];
									MBfloat.msb[1] = HoldingReg[0+ReadOffset];

									SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
									TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

								}else
								if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);


					break;

		case MB_FLOAT_TRD_FINT:// 0x18 = 19

			if(ReadAmount< 2)ReadAmount=2;
			if(priority <= PrioretyLevel)
				{
					//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
					if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
					{

						MBfloat.msb[1] = HoldingReg[1+ReadOffset];
						MBfloat.msb[0] = HoldingReg[0+ReadOffset];
					//	SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);

						MBfloat.flt *= 10;
						MBlong.lng =(unsigned long )MBfloat.flt;



						for(i=0;i<4;i++)
							TrendSendToOutpu(TREND_ADD, TrendRegNum+i , MBlong.bytes[i] );
						TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
					}else
						if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
				}



			break;

		case MB_FLOAT_TRD_FINT_INV: //0x28= 20


			if(priority <= PrioretyLevel)
				{
					//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
					if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
					{

						MBfloat.msb[1] = HoldingReg[0+ReadOffset];
						MBfloat.msb[0] = HoldingReg[1+ReadOffset];
					//	SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);

						MBfloat.flt *= 10;
						MBlong.lng =(unsigned long)MBfloat.flt;


						//MBlong.flt *= 10;
						for(i=0;i<4;i++)
							TrendSendToOutpu(TREND_ADD, TrendRegNum+i , MBlong.bytes[i] );

						TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

					}else
						if(ReportModbusFail)
							TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
				}





			break;


		case MB_INPUT_REG_TRD_INT:// 0x08 = 18


				if(priority <= PrioretyLevel)
					{
						//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
					if(ReadAmount< 2)ReadAmount=2;
					  if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
						{

							MBfloat.msb[1] = HoldingReg[1+ReadOffset];
							MBfloat.msb[0] = HoldingReg[0+ReadOffset];
						//	SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);

							//MBlong.flt = MBfloat.flt;
							//MBlong.flt *=10;

							SendToTrenFloat(TREND_ADD, TrendRegNum,HoldingReg[0]);

							TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

						}else
							if(ReportModbusFail)
									TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
					}


		break;



		case 	MB_INPUT_TRD_FLOAT_INV://0xf8 = 17
			if(ReadAmount < 2)ReadAmount= 2;
			if(priority <= PrioretyLevel)
								if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
								{
											MBfloat.msb[1] = HoldingReg[1+ReadOffset];
											MBfloat.msb[0] = HoldingReg[0+ReadOffset];
											SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
											TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

								}else
										if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);


			break;

		case MB_INPUT_TRD_FLOAT://0xf=16
			if(ReadAmount<2)ReadAmount=2;
				if(priority <= PrioretyLevel)
					if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
					{
								MBfloat.msb[0] = HoldingReg[1+ReadOffset];
								MBfloat.msb[1] = HoldingReg[0+ReadOffset];

								SendToTrenFloat(TREND_ADD, TrendRegNum,MBfloat.flt);
								TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

					}else
						if(ReportModbusFail)
								TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);



			break;




		case TRD_TO_MB_HOLD://0x60


			if(priority <= PrioretyLevel)
			//if(TrendReadByte(TREND_ADD,TrendRegNum, &ReadValue[0]))
			//{
			if(TrendReadReal(TREND_ADD,TrendRegNum, &MBfloat.flt))
			{
				TrendNum = (int)MBfloat.flt;
				if(trend_triger_not_to_send != TrendNum)
				if(MbSetRegisterValue(Modbuscardadd,Modbusreg,TrendNum) == false && ReportModbusFail)
				{
									TrendSendToOutpu(TREND_ADD, ReportModbusFail , 1);
									TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}
				else
					TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);
			}

			break;

		case TRD_TO_MB_COIL://0x50

			if(priority <= PrioretyLevel)
			//if(TrendReadByte(TREND_ADD,TrendRegNum, &ReadValue[0]))
			//{
			if(TrendReadReal(TREND_ADD,TrendRegNum, &MBfloat.flt))
			{
				TrendNum = (int)MBfloat.flt;

				if(trend_triger_not_to_send != TrendNum)
				if(MBSet8Coils(Modbuscardadd,Modbusreg,TrendNum)== false && ReportModbusFail)
				{
									TrendSendToOutpu(TREND_ADD, ReportModbusFail , 1);
									TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}else
					TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);
			}

			break;
		case TRD_TO_MB_DBL://0x40
			if(priority <= PrioretyLevel)
						if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
						{
							TrendSendToOutpu(TREND_ADD, TrendRegNum , HoldingReg[ReadOffset] & 0xfff);

						//if(HoldingReg[0] & 0xf000)
							TrendSendToOutpu(TREND_ADD, TrendRegNum+1 , (HoldingReg[ReadOffset] & 0xf000)>> 12);
							TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);

						}else
							TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);


			break;

		case MB_TO_TRD_INPUTS_PINS: // 0xa0: // read mb input to 2 trand bytes
			if(priority <= PrioretyLevel)
			{
				//unsigned int ReadData =	MBReadCoilsInputs(Modbuscardadd,Modbusreg,true);
				//if(MBReadCoilsInputs(Modbuscardadd,Modbusreg,0x4))
				if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD))
				{
					TrendSendToOutpu(TREND_ADD, TrendRegNum , HoldingReg[0+ReadOffset]  & 0xff);
					TrendSendToOutpu(TREND_ADD, TrendRegNum+1 , (HoldingReg[0+ReadOffset]  & 0xff00)>> 8);
					TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}else
					TrendSendToOutpuErr(TREND_ADD, ReportModbusFail ,Modbuscardadd);
			}

			break;
		case TRD_TO_MB_FLOATE:// 0xb0:
			if(priority <= PrioretyLevel)
									if(TrendReadReal(TREND_ADD,TrendRegNum, &MBfloat.flt))
									{
										//MBfloat.flt = ReadValue[0];
										//MBfloat.flt /= 10;
										if(trend_triger_not_to_send != MBfloat.flt)
										if(MbSetRegisterValue(Modbuscardadd,Modbusreg, MBfloat.msb[1]) == false && ReportModbusFail && MbSetRegisterValue(Modbuscardadd,Modbusreg+1, MBfloat.msb[0]) == false)
										{
													TrendSendToOutpu(TREND_ADD, ReportModbusFail , Modbuscardadd);
													TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);

										}else
											TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);
									}

			break;



		case TRD_TO_MB_FLOATE_REV:// 0xc0:
			if(priority <= PrioretyLevel)
						if(TrendReadReal(TREND_ADD,TrendRegNum, &MBfloat.flt))
						{

							if(trend_triger_not_to_send !=  MBfloat.flt)
							if(MbSetRegisterValue(Modbuscardadd,Modbusreg, MBfloat.msb[0]) == false && ReportModbusFail && MbSetRegisterValue(Modbuscardadd,Modbusreg+1, MBfloat.msb[1]) == false)
							{
										//TrendSendToOutpu(TREND_ADD, ReportModbusFail , 1);
								TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);

							}else
								TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
						}




			break;

		case TRD_TO_MB_COIL_SIGNAL://	0xd0:

			if(priority <= PrioretyLevel)
						//if(TrendReadByte(TREND_ADD,TrendRegNum, &ReadValue[0]))
						if(TrendReadReal(TREND_ADD,TrendRegNum, &MBfloat.flt))
						{
							TrendNum = (int)MBfloat.flt;

							if(trend_triger_not_to_send != TrendNum)
							 if(MBSetSingleCoil(Modbuscardadd,Modbusreg,(uint8_t)TrendNum) == false)
								 	 	 	 TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);
								 	 	 else
								 	 		 TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
						}


			break;
		case MB_TO_TRD_HOLD_TO_FLOATS://	0xe0:


			if(ReadAmount<2)ReadAmount= 2;
			if(priority <= PrioretyLevel)
						if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_HOLDING_CMD))
						{


							SendToTrenFloat(TREND_ADD, TrendRegNum,(float)HoldingReg[0+ReadOffset] );
							SendToTrenFloat(TREND_ADD, TrendRegNum+1,(float)HoldingReg[1+ReadOffset] );


							//TrendSendToOutpu(TREND_ADD, TrendRegNum ,   HoldingReg[0] );
							//TrendSendToOutpu(TREND_ADD, TrendRegNum+1 , HoldingReg[0] >> 8 );

							TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
						}else
							TrendSendToOutpuErr(true, ReportModbusFail , Modbuscardadd);




			break;

		case RESERV1:// -24

			//if(ReadAmount< 2)ReadAmount=2;
			if(priority <= PrioretyLevel)
			{
				if(MBReadOneHolding(Modbuscardadd,Modbusreg,ReadAmount,READ_INPUTS_CMD)) // Read before 2 registers
				{
					SendToTrenFloat(TREND_ADD, TrendRegNum,(float)HoldingReg[ReadOffset] );
					//TrendSendToOutpu(TREND_ADD, TrendRegNum , HoldingReg[0+ReadOffset] );
					TrendSendToOutpuErr(false, ReportModbusFail , Modbuscardadd);
				}else
					if(ReportModbusFail)TrendSendToOutpuErr(TREND_ADD, ReportModbusFail , Modbuscardadd);
			}


			break;







		case EOF:
			CmdPtr =START_OF_RECORDS;	PrioretyLevel++;  if(PrioretyLevel>2)PrioretyLevel=0;		break;
		default: CmdPtr = START_OF_RECORDS; break;
	}



}


	char TwobyteopFlag =0;

	void Write_trand(unsigned int reg,unsigned int val)
    {
		RunLeder =0 ;
		if(reg > 4900)
		{
			if(TwobyteopFlag)
			{
				MBfloat.msb[0] = val;
				TwobyteopFlag = 0;
			}else
			{
				MBfloat.msb[1] = val;
				SendToTrenFloat(TREND_ADD,reg - 5001,MBfloat.flt );
				TwobyteopFlag = 1;
			}

		}else
			SendToTrenFloat(TREND_ADD,reg - 4000,(float)val );
    }

    unsigned int Readfromtrand(unsigned int reg)
    {

    	unsigned int result;
    	RunLeder =0 ;
    	if(reg > 4999)
    	{
    		if(TwobyteopFlag)
    		{
    			result = MBfloat.msb[1];
    			TwobyteopFlag = 0;
    		}else
    		{
    			TrendReadReal(TREND_ADD,reg-5000, &MBfloat.flt);
    			TwobyteopFlag = 1;
    			result = MBfloat.msb[0];
    		}

    	}else
    	{
    		TrendReadReal(TREND_ADD,reg-4000, &MBfloat.flt);
    		result = MBfloat.msb[0];
    	}
    	return result;
    }



	
