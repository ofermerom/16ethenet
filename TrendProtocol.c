#include <stdio.h> 
 //#include <math.h>
 
 #include "Hardware.h"
//#include "globals.h"
void AnalogAndOutputTrd();
#define OUTPROC  AnalogAndOutputTrd()


#define RSTWD WDTCTL = WDT_ARST_1000

extern unsigned char portbuff[50],portptr;

 bool TrendWriteByte(int Address, int RamAddress, int RamValue);
 bool PortSendBytes(byte *data, int len ,int expected);
  

 void Float2Trn(float val, unsigned char *res) // conver 32bits float to trend
 {


 	res[2] = 0x40 ;// bit 6 allwayes 1
 	if (val < 0)
 	{
 		res[2] |= 0x20 ; // mantisa sign
 		val *= -1 ;
 	}


 	if(val < 100)
 	{

 	 	val *=100; // move val 2 digits
 	 	long m = (long)val;

 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb

 	 	//if(res[1] != 27)
 	 //	{
 	 		res[2] |= 0x10; //set  exponnet negative
 	 		res[2] |= 0x2; //set  exponnet 2 digit after 0
 	 //	}

 	}else
 		if(val < 1000)
 		{

 			val *=10; // move val 2 digits
 			 	 	long m = (long)val;

 			 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 			 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb

 			 	 		res[2] |= 0x10; //set  exponnet negative
 			 	 		res[2] |= 0x1; //set  exponnet 1 digit after 0

 		}
 		else
 			if(val < 10000)
 			{

 	 			//val *=1000; // move val 2 digits
 	 			 	 	long m = (long)val;

 	 			 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 	 			 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb

 	 			 	 		//res[2] |= 0x10; //set  exponnet negative
 	 			 	 	//	res[2] |= 0x1; //set  exponnet 2 digit after 0

 			}
 			else
 			{

 				int nv =0;
 				while(val > 999)
 				{
 					val /=10;
 					nv++;
 				}


 				long m = (long)val;
 				res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 				res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb
 				res[2] |=nv; //set  exponnet 2 digit after 0


 			}

 				/*
 				if(val < 100000)
 				 			{
 				 	 					val /=10; // move val 2 digits
 				 	 			 	 	long m = (long)val;
 				 	 			 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 				 	 			 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb
 				 	 			 	 	res[2] |= 0x1; //set  exponnet 2 digit after 0
 				 			}

 	 			else
 	 				if(val < 1000000)
 	 				 			{
 	 				 	 					val /=100; // move val 2 digits
 	 				 	 			 	 	long m = (long)val;
 	 				 	 			 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 	 				 	 			 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb
 	 				 	 			 	 	res[2] |= 0x3; //set  exponnet 2 digit after 0
 	 				 			}

 	 				else
 	 	 	 				if(val < 10000000)
 	 	 	 				 			{
 	 	 	 				 	 					val /=1000; // move val 2 digits
 	 	 	 				 	 			 	 	long m = (long)val;
 	 	 	 				 	 			 	 	res[0] = (unsigned char)(m / 100) + 27 ; // build trend mantisa msb
 	 	 	 				 	 			 	 	res[1] = (unsigned char)(m % 100) + 27 ; // build trend mantisa lsb
 	 	 	 				 	 			 	 	res[2] |= 0x4; //set  exponnet 2 digit after 0
 	 	 	 				 			}
 	 	 	 				else
 	 	 	 				{
 	 	 	 					n=0;
 	 	 	 					val /=1000; // move val 2 digits


 	 	 	 				}
*/




 }
  
  
void Int2Trn(int val, unsigned char *res )
{
	res[2] = 0x40 ;// all ways bit 6 "1"
	if (val < 0)
		{
			res[2] |= 0x20 ; //signed  of mantisa
			val *= -1 ;
		}

		res[0] = (unsigned char)(val / 100) + 27 ;
		res[1] = (unsigned char)((long)val % 100) + 27 ;
		if(val ==0)res[0] =res[1] =27;
}

void Double2Trn(double val, unsigned char *res)
{
	int i ;
	res[2] = 0x40 ;// all ways bit 6 "1"
	if (val < 0) 
	{
		res[2] |= 0x20 ; //signed  of mantisa
		val *= -1 ;
	}
	for (i = 0 ; i < 15 && val < 100 ; i++)
		val *= 10 ;
	if (i > 0)
		res[2] |= 0x10 | (unsigned char)i ;//signed of exponent bit 5
	else
	{
		for (i = 0 ; i < 15 && val > 10000 ; i++)
			val /= 10 ;
		res[2] |= (unsigned char)i ;// bit 0..3 the exponent
	}
	res[0] = (unsigned char)(val / 100) + 27 ;
	res[1] = (unsigned char)((long)val % 100) + 27 ;
}  
void Int2Tin(int val, unsigned char *res)
{
	res[0] = (unsigned char)(val / 100) + 27 ;
	res[1] = (unsigned char)((long)val % 100) + 27 ;
}  
  
void Trn2Double(unsigned char trn1, unsigned char trn2, unsigned char trn3, double *val)
{
	int i ;
	*val = trn2 - 27 ;
	*val += (trn1 - 27) * 100 ;
	if (trn3 != 0)
	{
		for (i = 0 ; i < (trn3 & 0xF) ; i++)
			if (trn3 & 0x10)
				*val /= 10 ;
			else
				*val *=10 ;
		if (trn3 & 0x20) 
			*val *= -1 ;
	}
}  
  
  
  
  
  
  
	int Truncate(double val)
	{
		return (int)val;
	}



	// {STX,32,1,'z','0','1',EOT,'0','0',24,0,0,0,0,0,64,ETX} ;
    bool TrendWriteInt(int Address, int RamAddress, int val)
        {
         int Len,Expect;
         byte s[30] ;
         bool result=true;
         s[0]=stx;
         s[1]=(byte)Address;// {Bakar address}
         s[2]=1;//       {My address}
         s[3]=(byte)'z';
         s[4] = (byte)'0';// 0xb0;// '0';
         s[5]=(byte)'2';
         s[6]=eot;
         s[7]=(byte)'0';
         s[8] =(byte)'0';
         s[9]=25;
         s[10]= (byte)((int) Truncate((double)RamAddress/100)+27);
         s[11]=(byte)((int)(RamAddress-100*(int) Truncate((double)RamAddress/100))+27);
         s[12] = (unsigned char)(val / 100) + 27 ;
         s[13] = (unsigned char)((long)val % 100) + 27 ;

                 //s[12]= (byte)((int) Truncate((double)ByteToSend/100)+27);
         //s[13]=(byte)((int)(RamAddress-100*(int) Truncate((double)ByteToSend/100))+27);
         s[14]=etx;
         Len = 15;// 17;
         Expect = 10;
         result = PortSendBytes(&s[0], Len, Expect);

          result =
          result    &&
          (s[0]==stx) &&
          //(s[2]==(byte)Address) &&
          (s[3]=='z') &&
          (s[4]=='0') &&
          (s[5]=='2') &&
          (s[6]== ack) &&
          (s[7]=='0') &&
          (s[8]=='0') &&
          (s[9]== etx);



          if(result)
          {
	        //  {write 0 to alarm -> because writing analog val causing an alarm in trend}

            /*
              for (i = 1; i < 5; i++)
              {
                  DelayMs(100);
                  //if IQ3 = 0 then  // if(IQ3 == 0)
                  if (TrendWriteByte(Address, RamAddress + 40, 0)) break;
              }*/
          }
          return result;
    }





#define CONTROLLER_ADD 0x1

        bool TrendWriteReal(int Address, int RamAddress, byte RealByte1, byte RealByte2, byte RealByte3)
        {
         int Len,Expect;
         byte s[30] ;                     
         bool result=true;            
         s[0]=stx;
         s[1]=	CONTROLLER_ADD;//		(byte)Address;// {Bakar address}
         s[2]=1;//       {My address}
         s[3]=(byte)'z';
         s[4] = (byte)'0';// 0xb0;// '0';
         s[5]=(byte)'2';
         s[6]=eot;
         s[7]=(byte)'0';
         s[8] =(byte)'0';
         s[9]=24;//     {for analog mode}
         s[10]= (byte)((int) Truncate((double)RamAddress/100)+27);
         s[11]=(byte)((int)(RamAddress-100*(int) Truncate((double)RamAddress/100))+27);
         s[12]=(byte)RealByte1;
         s[13]=(byte)RealByte2;
         s[14]=(byte)RealByte3;
         s[15]= 59;   // {alarm code ???}
         s[16]=etx;
         Len = 17;
         Expect = 10;


         result = PortSendBytes(&s[0], Len, Expect);
        
          result =
          result    &&
          (s[0]==stx) &&
          //(s[2]==(byte)Address) &&
          (s[3]=='z') &&
          (s[4]=='0') &&
          (s[5]=='2') &&
          (s[6]== ack) &&
          (s[7]=='0') &&
          (s[8]=='0') &&
          (s[9]== etx);

        
    
          if(result)
          {
	        //  {write 0 to alarm -> because writing analog val causing an alarm in trend}             
            
            /*
              for (i = 1; i < 5; i++)
              {
                  DelayMs(100);                                       
                  //if IQ3 = 0 then  // if(IQ3 == 0)
                  if (TrendWriteByte(Address, RamAddress + 40, 0)) break;
              }*/
          }
          return result;
    }


		bool TrendSendToOutpu(int Address, int RamAddress, int val)
		{
			
			// byte RealByte1, RealByte2,  RealByte3;
			//  RealByte1 = (byte)(Truncate((double)val / 100) + 27);
         	//  RealByte2 = (byte)((int)(val - 100 * (int)Truncate((double)val / 100)) + 27);
            //  RealByte3 = 0x4;
            //  if(val > 100)
           //   {
            //  	RealByte3 |= 0x11;
            //  }
           //   RealByte3 = 0x0;
			
              unsigned char bff[5];

              Int2Trn(val,bff);//	  Double2Trn((double) val,bff);
              return TrendWriteReal( Address,  RamAddress,  bff[0],  bff[1], bff[2]);
			 //return TrendWriteInt( Address ,RamAddress,val);//
			 //return TrendWriteReal( Address,  RamAddress,  RealByte1,  RealByte2,  RealByte3);

		}

		bool TrendSendToOutpuUI(int Address, int RamAddress,unsigned int val)
		{
			unsigned char bff[5];

			 Double2Trn((double) val,bff);
			return TrendWriteReal( Address,  RamAddress,  bff[0],  bff[1], bff[2]);


		}


		 bool SendToTrenFloat(int Address, int RamAddress,float flt)
		   {
		       		    unsigned char buff[5];
		       		    Float2Trn(flt,buff);
		      			return TrendWriteReal( Address,  RamAddress,  buff[0],  buff[1], buff[2]);
		   }




        bool TrendWriteByte(int Address, int RamAddress, int RamValue) 
        {
             int Len,Expect;
             byte s[50];// = new byte[50];
             bool  result =false;
       
             s[0]=stx;
             s[1] =(byte)Address;// {Bakar address}
             s[2] =1;     //  {My address}

             s[3] = (byte)'z';
             s[4] = (byte)'0';
             s[5] = (byte)'2';

             s[6]=eot;

             s[7] = (byte)'0';
             s[8] = (byte)'0';

             s[9] =	25;
             s[10] = (byte)((int)Truncate((double)RamAddress / 100) + 27);
             s[11] = (byte)((int)(RamAddress - 100 * Truncate((double)RamAddress / 100)) + 27);

             s[12] =(byte)((int) Truncate((double)RamValue / 100) + 27);
             s[13] = (byte)((int)(RamValue - 100 * Truncate((double)RamValue / 100)) + 27);
             s[14] = etx;

             Len=15;
             Expect = 10;

             result = PortSendBytes(&s[0], Len, Expect);
            
              result =
              result &&
              (s[0] == stx) &&
              //(s[2] ==(byte)Address) &&
              (s[3] == 'z') &&
              (s[4] == '0') &&
              (s[5] == '2') &&
              (s[6] == ack || s[6] == 0x15) &&
              (s[7] == '0') &&
              (s[8] == '0') &&
              (s[9] == etx);
            
            return result;
        }

		
		
		char blk;
		
       bool PortSendBytes(byte *data, int len ,int expected)
        {
    	   char i;
            bool result = true ;
		    int inf=0;
			byte *dpos;//,*clenpos;
			dpos = data;
			
                
                UCA0CTL1 |= UCSWRST; // reset serial port  
                for ( inf = 0; inf < 40; inf++)portbuff[inf]=0;// clean buffer                 
				UCA0CTL1 &= ~UCSWRST; 
				UCA0IE |= UCRXIE; 
				portptr =0;	
			 
			 	                                                             
            //    DtrEnable;
                while(len)
				{
					RSTWD;	 
					PortSendByte(*data++);
					len--;
				}


                for(i=0;i< 3;i++)
                {
                	OUTPROC ;
                	DelayMs(10);
                	OUTPROC ;
                }
              //   DtrDisable;
               
				
               int TimeOut = 170;/// 50;
                while (TimeOut > 0)
                {					
                    if (portptr >= expected) break;
                    for(i=0;i< 4;i++)
                    {
                    	OUTPROC ;
                    	DelayMs(5);//20
                    }
                    TimeOut--;
                }
                if (TimeOut == 0)
                	result = false;
                else
                {

                   if(portptr < expected)result = false;                     
                   for(inf = 0;inf < portptr;inf++)  
                     						*dpos++ = portbuff[inf];

                }

                if(result)
                {
                	if(blk)	Led(1,ON);else Led(1,OFF);
                	blk = blk ? 0:1;
                }else
                	 Led(1,OFF);

            return result;
        }







      bool TrendReadByteF(int Address,int RamAddress, unsigned int *ReturnValue )
        {
             int num =0;
             int  Len,Expect ;
             byte s[50];
             bool  result =false;                        
             result = false;             
             s[0] =stx;
             s[1] = (byte)Address; //{Bakar address}
             s[2] =1;//       {My address}
             s[3] =(byte)'z';
             s[4] = (byte)'0';// 0xb0;// '0';
             s[5]=(byte)'1';
             s[6] =  enq;//0x85
             s[7] = (byte)'0';
             s[8] = (byte)'0';
             s[9] = 24;// 25;
             s[10]= (byte)((int)Truncate((double)RamAddress / 100) + 27);
             s[11] = (byte)((int)(RamAddress - 100 * Truncate((double)RamAddress / 100)) + 27);
             //s[12] = 0x9d;//
             s[12] =  etx;
             Len =13;
             Expect =12;//12
             RSTWD;	 
             OUTPROC;

             result = PortSendBytes(&s[0],Len,Expect);
                        
             RSTWD;	 
             result  =
             result &&
             (s[0]==stx)      &&
             (s[1]== 1)      &&
             //(s[2]==(byte)Address)  &&
             (s[3]== (byte)('z')) &&
             (s[4]==(byte)('0')) &&
             (s[5]==(byte)('1')) &&
             (s[7]==(byte)('0')) &&
             (s[8]==(byte)('0')) &&
             (s[11]&& 0x80 ) &&              
             (s[13]==etx);


		
             if (result)
             {             	
             	 num = (s[9] - 27) * 100 + s[10] - 27;             	
             	 if ((s[11] & 0x20) > 0)
             	 {             	 
             	 	 num *= -1;
             	 	 //result = 0; // no negative numbers
             	 }
                 if ((s[11] & 0x10) > 0)
                 {
                     switch(s[11] & 0xf)
                     {
                     	case 0: break;
                     	case 1: num /=10;  break;
                     	case 2: num /=100;  break;
                     	case 3: num /=1000;  break;                     	
                     }
                     
                     //num = num * Pow(10, -(s[10] & 0xf));
                 }
                 else
                 {
                 	switch(s[11] & 0xf)
                     {
                     	case 0: break;
                     	case 1: num *=10;  break;
                     	case 2: num *=100;  break;
                     	case 3: num *=1000;  break;                     	
                     }                 	
                     //num = num * Pow(10, (s[10] & 0xf));
                 }
             	             	
             	  	*ReturnValue = num;
             	
             	
             	    
             }else
             {
             	result = false; 	
             }                 
            return result;
        }



      bool TrendReadByte(int Address,int RamAddress, unsigned int *ReturnValue )
          {
        	  bool result = false;
        	  unsigned int value[4] ={0,0,0};
        	  int i=0;
        	  for(i=0;i<5;i++)
        	  {
        		  result = TrendReadByteF( Address, RamAddress,  &value[0] );
        		  *ReturnValue = value[0];
        		  if(result)break;
        	  }



        	  return result;


          }



      bool TrendReadRealF(int Address,int RamAddress, float *ReturnValue )
              {
                   int num =0;
                   int  Len,Expect ;
                   byte s[50];
                   bool  result =false;
                   result = false;
                   s[0] =stx;
                   s[1] = (byte)Address; //{Bakar address}
                   s[2] =1;//       {My address}
                   s[3] =(byte)'z';
                   s[4] = (byte)'0';// 0xb0;// '0';
                   s[5]=(byte)'1';
                   s[6] =  enq;//0x85
                   s[7] = (byte)'0';
                   s[8] = (byte)'0';
                   s[9] = 24;// 25;
                   s[10]= (byte)((int)Truncate((double)RamAddress / 100) + 27);
                   s[11] = (byte)((int)(RamAddress - 100 * Truncate((double)RamAddress / 100)) + 27);
                   //s[12] = 0x9d;//
                   s[12] =  etx;
                   Len =13;
                   Expect =12;//12
                   RSTWD;
                   OUTPROC;

                   result = PortSendBytes(&s[0],Len,Expect);

                   RSTWD;
                   result  =
                   result &&
                   (s[0]==stx)      &&
                   (s[1]== 1)      &&
                   //(s[2]==(byte)Address)  &&
                   (s[3]== (byte)('z')) &&
                   (s[4]==(byte)('0')) &&
                   (s[5]==(byte)('1')) &&
                   (s[7]==(byte)('0')) &&
                   (s[8]==(byte)('0')) &&
                   (s[11]&& 0x80 ) &&
                   (s[13]==etx);



                   if (result)
                   {
                   	 num = (s[9] - 27) * 100 + s[10] - 27;
                   	 if ((s[11] & 0x20) > 0)
                   	 {
                   	 	 num *= -1;
                   	 //	 result = 0; // no negative numbers
                   	 }

                   	 int nb = s[11] & 0xf; // th expo
                   	 float rl =(float)num;
                   	 if(s[11] & 0x10) // expo is negativ
                   	 {
                   		 while(nb > 0)
                   		 {
                   			 rl /=10;
                   			 nb--;
                   		 }

                   	 }else
                   	 {
                   		 while(nb > 0)
                   		 {
                   		       rl *=10;
                   		       nb--;
                   		 }

                   	 }


                  	*ReturnValue = rl;



                   }else
                   {
                   	result = false;
                   }
                  return result;
              }



      bool TrendReadReal(int Address,int RamAddress, float *ReturnValue )
         {
       	  	  	  bool result = false;
       	         	  float value =0;
       	         	  int i=0;
       	         	  for(i=0;i<5;i++)
       	         	  {
       	         		  result = TrendReadRealF( Address, RamAddress,  &value );
       	         		  *ReturnValue = value;
       	         		  if(result)break;
       	         	  }



       	         	  return result;

         }

