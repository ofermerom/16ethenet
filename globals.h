/*
 * globals.h
 *
 *  Created on: 25 ���� 2012
 *      Author: ofer.m
 */

//#ifndef GLOBALS_H_
//#define GLOBALS_H_

#define DI16


#define VERSION_ 225




#define uint16_t unsigned int
#define uint8_t unsigned char

//#define USHORT unsigned int
#define USHORT unsigned short

#define ETHERNET


#define UCHAR unsigned char

#define uchar unsigned char
#define true 1
#define false 0
#define TRUE 1
#define FALSE 0

#define int8 char 
#define int16 int
#define uint8 unsigned char 
#define uint16 unsigned int 
#define Word unsigned int
#define uint unsigned int

#define bool char 


#define EXLERATOR


#ifdef EXLERATOR

#define OUTPROC  AnalogAndOutputTrd()

#else
#define OUTPROC  nullfunc()

#endif




#define INPUT_TH 100


void SetRlyOn(int rly);

void uartEnableTx(char st);
void MBUartRecData(UCHAR data);
void MBtimerCall(void);
void HoteloOneSecCall();
void putcx(uchar ch ,uchar uart);

//void putc(char ch);
void SetRlyOff(int rly);
void delay(unsigned int times);
void ldrun();

#ifdef MAIN

uchar cardaddres=0,coils[50],LEDSreg =0,RLYreg = 0,MasterMode = 0,CfgInpus16 =0 ;//,InputsHReg =0;//,STxbuf[30];
volatile unsigned int ADC_Result[19],RLY_PWM_TM=0,active_adc_ch=0,adc_mem=0,HoldingReg[100];
//int TxDataLen = 0,TxDataPtr =0;
volatile unsigned int OneSecTimer =0,Gpflags =0,OpMode =0;

#else

//extern int TxDataLen = 0,TxDataPtr =0;
extern uchar cardaddres,coils[50],LEDSreg ,RLYreg,MasterMode,CfgInpus16;//,InputsHReg =0 ;//,STxbuf[30];
extern volatile unsigned int ADC_Result[19],RLY_PWM_TM,active_adc_ch,adc_mem,HoldingReg[100];
extern volatile unsigned int OneSecTimer,Gpflags,OpMode;
#endif




//#endif /* GLOBALS_H_ */


#define IN1 BIT0
#define IN2 BIT1
#define IN3 BIT2
#define IN4	BIT3



#define READ_HOLDING_CMD	0x03
#define READ_INPUTS_CMD      0x04



#define RSTWD //WDTCTL = WDT_ARST_1000



inline int GetRegBit(unsigned int xbit)
{
	return (Gpflags & xbit) ? 1:0; 	
}

inline void ClrRegBit(unsigned int xbit)
{
	Gpflags &=~xbit;
}

inline void SetRegBit(unsigned int xbit)
{
	Gpflags |= xbit;		
}
char CheckLdScr();
void SetLeds(uchar led,uchar OnOff);
void SetRly(uchar rly,uchar OnOff);
void AdcProc(void);
void CmdExMachine();
void ProcMbMasterHostReplay();
uint8_t MBReadHoldings(uint8_t hostid);
uchar  MbSetRegisterValue(uchar hostid,unsigned int RegAdd,uint data);
uint16_t MBReadOneHolding(uint8_t hostid,unsigned int RegAdd,uint8_t len,uint8_t cmd);
unsigned int ConverTrendValue(unsigned int ReadValue);
bool TrendSendToOutpu(int Address, int RamAddress, int val);
void GetCardAdd();
void SpiUpdate();
char checkforinputschange(char uppers);
char CheckOneChange(int add);
void AnalogAndOutputTrd();
char LoadLdScript();
uint8_t MBSet8Coils(uint8_t hostid,uint16_t RegAdd,uint8_t val);
uint16_t MBReadCoilsInputs(uint8_t hostid,uint16_t RegAdd,uint8_t inputs);
void LoadAnalogThVals();
void trendprintf(char *buff);
void DelayUs(uint len);
void  LoadUartConfig();
char GetPgmSwitchStat();
void Write_trand(unsigned int reg,unsigned int val);
unsigned int Readfromtrand(unsigned int reg);
void EthernetProc();
void ModBusTcpProc();
