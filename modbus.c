
#include "hardware.h"
#include "globals.h"
#include "modbusdef.h"
#include "eeprom.h"



//#define BANK_HP

extern char CfgProgMaode,UartDelayBtweenTxBytes ;
uint8_t GetCoilStat(int coil);
int WaitForClientTimeOut = 500;
volatile int MasterTimeOutTimer=0,rs_dir_interval=1;
volatile char SlaveRespons = 0;
extern uchar InputsHReg;
extern int CardConfigReg;
#define REC_MSG_INTERVAL 45
unsigned char Kepress =0;
USHORT
usMBCRC16( UCHAR * pucFrame, USHORT usLen );
void SetBitRegs(uint add, bool val);
bool GetBitReg(int add);

typedef enum
{
    STATE_SLAVE_IDLE,
	STAT_SLAVE_REC,
	STATE_SLAVE_MSG,
	STATE_SEND_DATA,
	STATE_PROC_MSG
} MBSlaveState;


union
{
    float fl;
    unsigned int msbint[2];
    unsigned char bytea[4];

}ftype;



int TxDataLen = 0,TxDataPtr =0;

volatile MBSlaveState SlaveRecStat = STATE_SLAVE_IDLE;
MBSndState SlaveTxStat =	STATE_TX_IDLE;
uint8_t DemmyLen = 0;
uint8_t RecTimer =0;


uint8_t STxbuf[100],STxPtr=0;
uint8_t SMbRecBuffer[150],SMbRxptr =0,ModbusRecBytes =0;



#define MB_SLAVE_LAST_ADD 52
#define MAX_MB_RTU_FRAM_SIZE 100
void MbStxdata(uint8_t datalen);
void MBSlavesend(uint8_t datalen);



int lastADC_Result[20];

char CheckOneChange(int add)
{
	int diff =0,change =0;
	AdcProc();
	diff = ADC_Result[add] - lastADC_Result[add];
	if(diff < 0)diff *=-1;
			if(diff > 3)
			{
				lastADC_Result[add] = ADC_Result[add];
				change=1;
			}

	return change;
}


char checkforinputschange(char uppers)
{
	char i=0,range = 8,strat =0;//,pos =0x1;
	int diff;
	 AdcProc();
	 if(uppers)
	 {
		 range = 17;
		 strat = 9;
	 }else
	 {
		 range = 8,
		 strat =0;

	 }

	 if(uppers == 2)
		 strat = 0;
	for(i=strat;i<range;i++)
	{
		diff = ADC_Result[i] - lastADC_Result[i];
		if(diff < 0)diff *=-1;


//		if(diff > AnalogChangeTrig)// was diff > 3
	//	{
		//	lastADC_Result[i] = ADC_Result[i];
			//change=1;
	//	}

	}
	return 1;
}






void ReportCoilsStata(uint16_t coiladd, uint8_t amount,uint8_t function)
{
	uint8_t coil,datalen=0,bytestosend = amount/8+1,bpos ;
	STxbuf[datalen++] =	 cardaddres;
	STxbuf[datalen++] =	function;//	 MB_READ_COIL_CMD;
	if(coiladd > 9999)coiladd -=10000;
//	if(function == 0x1)
		STxbuf[datalen++] =  bytestosend;
//	if(function == 0x02)
//	{
	//	STxbuf[datalen++] = 0;
	//	STxbuf[datalen++] = coiladd;
	//	STxbuf[datalen++] = 0;
	//	STxbuf[datalen++] = amount;
	//}


	while(bytestosend)
	{
		bpos = 0x1;
		coil = 0;
		while(TRUE)
		{
			if( GetCoilStat(coiladd++))coil |= bpos ;
			if(bpos == 0x80)break;
			bpos <<=1;
		}
		STxbuf[datalen++] = coil;
		bytestosend--;
	}


	MbStxdata(datalen);

}






void  SetCoilStat(uint16_t coil,uint16_t val)
{
	if(coil > 99  && coil < 116)
		coil -=100;

	if(coil < 30)coils[coil] = val;
	if(coil < 8)SetRly(coil,val);	
	if(coil > 7 && coil < 16)SetLeds(coil - 8,val);
	
	
	switch(coil)
	{
	 case 25:    V_SHABAT(val); break;
	 case 26:    V_PULLUPS(val); break;
	 case 27:    LED1(val); break;
	 case 28:    LED2(val); break;
	 case 29:    LED3(val); break;


	}

	if(coil > 199 && coil < 250)
		SetBitRegs(coil-200 ,  val);
	if(coil > 299)SetBitRegs(coil-300 ,  val);

}

uint16_t THlow[8],THhi[8];
uint8_t  holdval[8];

void LoadAnalogThVals()
{
	int i=0;
	for(i=0;i< 8;i++)
	{
		THlow[i] = read_eeprom_word(2501 +i*2 );
		THhi[i]= read_eeprom_word(2500 +i*2 );
	}

}

uint8_t GetCoilStat(int coil)
{
	uint8_t val =0 ;

	if(coil > 99  && coil < 116)
			coil -=100;
	switch(coil)
		{
		//case 1: val = 82; break;
		case 8: val = Kepress & BIT0 ? 1:0 ; Kepress &= ~BIT0; break;
		case 9: val = Kepress & BIT1 ? 1:0 ; Kepress &= ~BIT1;break;
		case 10: val = Kepress & BIT2 ? 1:0 ; Kepress &= ~BIT2;break;
		case 11: val = Kepress & BIT3 ? 1:0 ; Kepress &= ~BIT3;break;
		case 12: val = Kepress & BIT4 ? 1:0 ; Kepress &= ~BIT4;break;


		case 25: val = (P2OUT & BIT5) ? 1:0 ;  break;
		case 26: val = (P4OUT & BIT0) ? 1:0 ;  break;
		case 30: val = CfgInpus16 ? 1:0; 	   break;

		}

	if(coil < 8) val =  P6IN & ( 0x1 << coil) ? 1:0  ;
	//if(coil > 7 && coil < 16){coil-=8 ;val =P1IN  & ( 0x1 << coil) ? 1:0  ;}
	if(coil > 15 && coil < 24){coil-=16 ;val =  InputsHReg & ( 0x1 << coil) ? 1:0  ;}
	
	if(coil > 199 && coil < 250)
				val = GetBitReg(coil-200);
	if(coil > 299)val = GetBitReg(coil-300);

	if(coil > 115 && coil < 124)// return analog TH values
	{
		coil -=116;
		if(ADC_Result[coil] > THhi[coil])
							holdval[coil] = 1;
		if(ADC_Result[coil] < THlow[coil])
									holdval[coil] = 0;

			val = holdval[coil];

	}


	return val;
}

char ProgEpEnb = false;
uint16_t GetHoldingValue(unsigned int reg)
{
	uint16_t val=0;
	
	if(reg > 99 && reg < 117)
		val = ADC_Result[reg - 100];
	
	switch(reg)
	{
		case 60: val = VERSION_; break;
		case 99: val = CfgInpus16 ? 1:0;	   break;
		case 117:val  = LEDSreg; break;
		case 118:val  = RLYreg;  break;
		case 119:val = TA0CCR1 ; break;
		case 120:val=  TA0CCR2 ; break;
		//case 121:val=  TA0CCR3 ; break;
		case 122:val = (P2OUT & BIT5) ? 1:0 ;  break;
		case 123:val = (P4OUT & BIT0) ? 1:0 ;  break;
		case 124:val = checkforinputschange(2); break;
		//case 125: val = AnalogChangeTrig; break;
		case 126: val = checkforinputschange(0); break;
		case 127: val = checkforinputschange(1); break;
		case 128: val = CardConfigReg; 			 break;
		case 129: val = rs_dir_interval; 		break;
		case 130: val = WaitForClientTimeOut;   break;
		case 131: val = read_eeprom_word(2020); break;
		case 132: val = read_eeprom_word(2021); break;
		case 133: val = read_eeprom_word(2022); break;
		case 134: val = read_eeprom_word(2009); break;
		case 149: val = ProgEpEnb; break;
		default: if(reg < 250) val = read_eeprom_word(reg); break;
	}
	

 //if(CfgProgMaode) // sherga mode
	 if(reg > 3999)val = Readfromtrand(reg );
	 	 else
	if(reg > 249)
			val = read_eeprom_word(reg-250);

	return val;
}


//uint16_t LastHolding[20];
//uint16_t SystemVars[20];

uint16_t Last_Rly = 0;

char	RunLeder = false;

void SetHoldingReg(uint16_t reg,uint16_t val)
{
	
	
//	if(LastHolding[reg - 115] == val)
//	{
		switch(reg)
		{			
			case 117:  LEDSreg = val; break;
			case 118:
			//if(Last_Rly == val) //6/8/2013
			//{
					RLYreg = val ;SpiUpdate();
			//}else
			Last_Rly = val;
			break;
			case 119:  TA0CCR1 = val; break;
			case 120:  TA0CCR2 = val; break;
			//case 121:  TA0CCR3 = val; break;
			case 122: V_SHABAT(val);break;
			case 123: V_PULLUPS(val); break;
			//case 125: AnalogChangeTrig = val; break;
			case 129: rs_dir_interval = val;write_eeprom_word(2005,val); break;
			case 130: WaitForClientTimeOut = val;write_eeprom_word(2007,val); break;
			case 131: write_eeprom_word(2020,val); break; // configura modbus boud rate
			case 132: write_eeprom_word(2021,val); break;
			case 133: write_eeprom_word(2022,val); break;



			case 134: write_eeprom_word(2009,val);UartDelayBtweenTxBytes = val; break;

		}



		 if(reg == 149)
		    {
		        if(val == 4321)
		        {
		            ProgEpEnb = true;
		            RunLeder = false; // stop run till program end
		        }
		        else
		            RunLeder =  CheckLdScr();

		    }


		 	 if( reg > 3999 ) // shraga mode
		 	 {
		 		  Write_trand(reg,val);
		 	 }
		 	 else
		 		 if(reg > 249 && ProgEpEnb )
		 			 write_eeprom_word(reg-250,val);
		   // else
		    //	 Write_trand(reg,val);

		    if(reg > 199 && reg < 250 && ProgEpEnb )
		    		write_eeprom_word(reg,val);

		  //  if(reg > 199 && reg < 215) SystemVars[reg-200]=val;



	//}

	//LastHolding[reg - 115] = val;
}






void modbusprocdata(void)
{

	uint16_t address,coils,data;
	 uint8_t datalen =0,i,bpos,j;
		switch(SlaveRecStat)
		{
			case STATE_SLAVE_MSG:
					if(usMBCRC16( &SMbRecBuffer[0], ModbusRecBytes) == 0)
					{
						//BlinkLed2;
						LED2ON;
						ModbusRecBytes = 0;
						address = SMbRecBuffer[MB_FUNCTION_CODE_POS+1]*0x100;
						address += SMbRecBuffer[MB_FUNCTION_CODE_POS+2];
						coils =	SMbRecBuffer[MB_FUNCTION_CODE_POS+3]*0x100;
						coils +=	SMbRecBuffer[MB_FUNCTION_CODE_POS+4];

						switch(SMbRecBuffer[MB_FUNCTION_CODE_POS])
						{

							case MB_READ_COIL_CMD:
								ReportCoilsStata(address,coils,MB_READ_COIL_CMD);
							break;

							case MB_INPUTS_CMD:
							 	ReportCoilsStata(address,coils,MB_INPUTS_CMD);
						     break;



							case MB_WRITE_SINGLE_COIL:
								SetCoilStat( address, coils > 0 ? 1:0);
								STxbuf[datalen++] = cardaddres;
								STxbuf[datalen++] = MB_WRITE_SINGLE_COIL;
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+1];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+2];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+3];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+4];
								MbStxdata(datalen);

							break;

							case MB_WRITE_MUL_COILS:
								STxbuf[datalen++] = cardaddres;
								STxbuf[datalen++] = MB_WRITE_MUL_COILS;
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+1];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+2];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+3];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+4];
								i = SMbRecBuffer[MB_FUNCTION_CODE_POS+5];
								j =0;
								while(i)
								{
								    bpos = 0x1;
									while(TRUE)
									{
									 SetCoilStat(address++, (SMbRecBuffer[MB_FUNCTION_CODE_POS+7+j] & bpos) ? 1:0);
									 if(bpos == 0x80)break;
									 bpos <<=1;

									}
								  j++;
								  i--;
								}
	//***********************************************************************************************
#ifdef BANK_HP

#else

								MbStxdata(datalen);
#endif
							break;



							case MB_READ_HOLDING_CMD:
								STxbuf[datalen++] = (cardaddres & 0xf);
								STxbuf[datalen++] = MB_READ_HOLDING_CMD;
								STxbuf[datalen++] = coils*2;
								if(address > 39000)address-=40000;
								for(i=0;i<coils;i++)
								{
									data = GetHoldingValue(address++);
									STxbuf[datalen++] = (data & 0xff00)>>8;
									STxbuf[datalen++] = data & 0xff;
								}

								MbStxdata(datalen);
							break;



							case MB_WRITE_SINGLE_REG:
								SetHoldingReg(address,coils);
								STxbuf[datalen++] = cardaddres;
								STxbuf[datalen++] = MB_WRITE_SINGLE_REG;
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+1];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+2];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+3];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+4];

#ifdef BANK_HP

#else
								MbStxdata(datalen);
#endif
							break;

							case MB_WRITE_MUL_REG:


								STxbuf[datalen++] = cardaddres;
								STxbuf[datalen++] = MB_WRITE_MUL_REG;
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+1];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+2];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+3];
								STxbuf[datalen++] = SMbRecBuffer[MB_FUNCTION_CODE_POS+4];
								i =0;


								while(coils)
									{
										SetHoldingReg(address++,SMbRecBuffer[MB_FUNCTION_CODE_POS+6+i]*0x100 + SMbRecBuffer[MB_FUNCTION_CODE_POS+7+i]  );
										i+=2;
										coils--;

									}
#ifdef BANK_HP

#else

								MbStxdata(datalen);
#endif
							break;


						}

					}

					else
						{
							SlaveRecStat = STATE_SLAVE_IDLE;
							LED2OFF;
						}
			break;



			default:
			 SlaveRecStat = STATE_SLAVE_IDLE; break;

		}
		SlaveRecStat ==	STATE_SLAVE_IDLE;
		SMbRxptr = 0;
	}

void NonModbusMsg()
{
	if(SMbRecBuffer[0] > 0x2a && SMbRecBuffer[0] < 0x3a && (SMbRecBuffer[1]& 0xf0)== 0xf0 )
	{
	
		SetCoilStat(SMbRecBuffer[0] - 0X30,SMbRecBuffer[1] & 0X1);
		SetCoilStat(SMbRecBuffer[0] - 0X30+8,SMbRecBuffer[1] & 0X1);
			
		
	}


	
}

void TcpTimerCall();

void MBtimerCall(void)
{
	 TcpTimerCall();
	if(SlaveRecStat ==	STAT_SLAVE_REC)
	{
		if(RecTimer > REC_MSG_INTERVAL)
		{
			//NonModbusMsg();
			SlaveRecStat = STATE_SLAVE_MSG;
			SlaveRespons = true;

			if((cardaddres & 0x3f )== SMbRecBuffer[MB_HOST_ID_POS])
			{
				Led(2,ON);
				ModbusRecBytes = SMbRxptr;				
				if(MasterMode)
					SlaveRespons = true;
				//else
					//modbusprocdata();
				
			}else
			{
				SlaveRecStat = STATE_SLAVE_IDLE;
				SMbRxptr =0;	
			}
			SMbRxptr =0;	
			//modbuspull();
		}else RecTimer++;

		

	}

	if(MasterTimeOutTimer > 0)MasterTimeOutTimer--;

}

void MBUartRecData(UCHAR data)
{
	SMbRecBuffer[SMbRxptr++] = data;
	if(SMbRxptr > 120)SMbRxptr = 0;
	//SlaveRespons = true;
	//if(SlaveRecStat ==	STATE_SLAVE_IDLE)
	SlaveRecStat =	STAT_SLAVE_REC;
	RecTimer = 0;
}




void MbStxdata(uint8_t datalen)
{

	SlaveRecStat = STATE_PROC_MSG;

	DelayMs(1);
	uartEnableTx(TRUE);
	DelayMs(6);
	uint16_t usCRC16 = usMBCRC16( &STxbuf[0], datalen);

	STxbuf[datalen++] =	 ( UCHAR )( usCRC16 & 0xFF ); // add CRC to packet
	STxbuf[datalen++] =  ( UCHAR )( usCRC16 >> 8 );
	TxDataLen = datalen;// -1;
	TxDataPtr =0;

	while(TxDataLen)
	{
		putcx(STxbuf[TxDataPtr++],UART2);
		TxDataLen--;

	}
	DelayMs(4); // change for 9600
	uartEnableTx(false);
	DelayMs(1);
	//MBSLAVEPORT = STxbuf[TxDataPtr++];

}


//**********************Master Command*********************


void MB_Master_sendhost(uint8_t datalen)
{
	uint8_t   Txptr = 0;

	uartEnableTx(TRUE);
	//DelayMs(1);// was 6

	uint16_t usCRC16 = usMBCRC16( &STxbuf[0], datalen);

	STxbuf[datalen++] =	 ( UCHAR )( usCRC16 & 0xFF ); // add CRC to packet
	STxbuf[datalen++] =  ( UCHAR )( usCRC16 >> 8 );	
 	 			 
	Txptr = 0 ;

	//datalen++;
	while(datalen)
	{	
		RSTWD;
		putcx(STxbuf[Txptr++],UART2);
		datalen--;
	}		
	SMbRxptr=0;
	SMbRecBuffer[0] = 0;

	delay(95+rs_dir_interval);//20
	uartEnableTx(false); // disable RS485 buffer 

	SMbRxptr = 0;

	Txptr =0;
	RecTimer =0;
	//DelayMs(2);//4

	SMbRxptr = 0;
	
	
	
	
	
	//WaitFromHostReplayTimeOut = WAIT_HOST_REP_TO_INTERVAL;
}






uint8_t MBReadHoldings(uint8_t hostid)
{

	uint16_t tmp;
	uint8_t datalen = 0,result = false;
	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = MB_READ_HOLDING_CMD;
	STxbuf[datalen++] = 0; 
	STxbuf[datalen++] = 100; // start address to get 
	STxbuf[datalen++] = 0;
	STxbuf[datalen++] = 8; //read 8 holding regs  
 	SMbRxptr = 0;
 	SMbRecBuffer[0] = 0;
 	MB_Master_sendhost(datalen);

	MasterTimeOutTimer = WaitForClientTimeOut;
	while(MasterTimeOutTimer >0)
	{
		if(SlaveRespons && (SMbRecBuffer[0] == hostid) && (SMbRecBuffer[1] == MB_READ_HOLDING_CMD))
		{
			
			for(datalen =0;datalen<23;datalen++)
			{
				tmp = SMbRecBuffer[datalen*2 +3];
				HoldingReg[datalen] = (tmp  << 8);
				HoldingReg[datalen] += SMbRecBuffer[datalen*2 +4];


			}		
									
			result = true;	
			SlaveRespons =0;
			break;
		}
			MasterTimeOutTimer--;	
	}
	


	SlaveRecStat ==	STATE_SLAVE_IDLE;
		
	return result;
}





uint16_t MBReadOneHolding(uint8_t hostid,unsigned int RegAdd,uint8_t len,uint8_t cmd)
{

	uint16_t tmp=0;
	uint8_t datalen = 0,result = false,tcmd = cmd;


	//uint8_t cmd = MB_READ_HOLDING_CMD;
	//if(	RegAdd > 39000)
//	{
	//	cmd = MB_READ_INPUTS_CMD;
	//	RegAdd -= 40000;
	//}

	for(datalen=0;datalen<10;datalen++) SMbRecBuffer[datalen]=0;
	datalen=0;



	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = tcmd;//	MB_READ_HOLDING_CMD;
	STxbuf[datalen++] = (RegAdd & 0xff00)>>8 ;
	STxbuf[datalen++] = RegAdd & 0xff; // start address to get
	STxbuf[datalen++] = 0;
	STxbuf[datalen++] = len; //reg count 
 	SMbRxptr = 0;
 	SlaveRespons = 0;
 	SMbRecBuffer[0] =0;
 	MB_Master_sendhost(datalen);

	MasterTimeOutTimer = WaitForClientTimeOut;
	while(MasterTimeOutTimer >0)
	{/*********************************************SMbRecBuffer[0] == STxbuf[0]****************/
		if(SlaveRespons /*&&  SMbRecBuffer[0] == hostid */&& SMbRecBuffer[1]  == tcmd  &&  SMbRecBuffer[2] == (len*2))// MB_READ_HOLDING_CMD)
		{
			Led(2,ON);
			for(datalen =0;datalen < len;datalen++)
			{
				tmp = 	(uint16_t)SMbRecBuffer[datalen*2 +3] ;
				HoldingReg[datalen] = (tmp  << 8);
				HoldingReg[datalen] += (uint16_t)(SMbRecBuffer[datalen*2 +4]);
			}		
									
			result = true;	
			SlaveRespons =0;
			break;
		}
			MasterTimeOutTimer--;	
			
			DelayMs(10);
	}
	


	SlaveRecStat ==	STATE_SLAVE_IDLE;
		
	return result;
}










uchar  MbSetRegisterValue(uchar hostid,unsigned  int RegAdd,uint data)
{
	uint8_t datalen = 0,result = false;
	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = MB_WRITE_SINGLE_REG;
	STxbuf[datalen++] = (uchar)((RegAdd & 0xff00)>>8); 
	STxbuf[datalen++] = (uchar)((RegAdd & 0xff));	 
	STxbuf[datalen++] = (uchar)((data & 0xff00)>>8);;
	STxbuf[datalen++] = (uchar)((data & 0xff));	 
 	
 	SlaveRespons =0;
 	MB_Master_sendhost(datalen);
 	SMbRxptr = 0;
 	
 	
 	MasterTimeOutTimer = WaitForClientTimeOut;

#ifdef BANK_HP
 	result = true;
    SlaveRespons =0;
#else
	while(MasterTimeOutTimer >0)
	{
		if(SlaveRespons && SMbRecBuffer[0] == STxbuf[0] && SMbRecBuffer[1] == STxbuf[1])
		{
														
			result = true;	
			SlaveRespons =0;
			break;
		}
				
	}
#endif
	
	SlaveRecStat ==	STATE_SLAVE_IDLE;
		 	
 	
 	return result;	
	
}


char ReadSucessFlage = 0;

uint16_t MBReadCoilsInputs(uint8_t hostid,uint16_t RegAdd,uint8_t inputs)
{

	uint8_t datalen = 0,cmd = inputs ? MB_INPUTS_CMD :MB_READ_COIL_CMD ;
	if(inputs > 2)cmd = inputs;
	uint16_t result = false;

	ReadSucessFlage =0;
	for(datalen=0;datalen<10;datalen++) SMbRecBuffer[datalen]=0;
	datalen=0;

	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = cmd;
	STxbuf[datalen++] = RegAdd >> 8;
	STxbuf[datalen++] = RegAdd; // start address to get
	STxbuf[datalen++] = 0;
	STxbuf[datalen++] = 8;// inputs > 2 ? 1  : 16; //reg count
 	SMbRxptr = 0;
 	SlaveRespons = 0;
 	SMbRecBuffer[0] =0;
 	MB_Master_sendhost(datalen);

	MasterTimeOutTimer = WaitForClientTimeOut;
	while(MasterTimeOutTimer >0)
	{/*********************************************SMbRecBuffer[0] == STxbuf[0]****************/

		if(SlaveRespons &&  SMbRecBuffer[0] == hostid && (SMbRecBuffer[1] & 0x7f) == cmd)
		{
			ReadSucessFlage =1;

			result = SMbRecBuffer[3];
			result <<=8;
			result |= SMbRecBuffer[4];

			 HoldingReg[0] = result;
			SlaveRespons =0;
			result = true;
			break;
		}
			MasterTimeOutTimer--;

			DelayMs(10);
	}


	SlaveRecStat ==	STATE_SLAVE_IDLE;

	return result;
}



uint8_t MBSet8Coils(uint8_t hostid,uint16_t RegAdd,uint8_t val)
{
	uint8_t datalen = 0,result=false;
		for(datalen=0;datalen<10;datalen++) SMbRecBuffer[datalen]=0;
			datalen=0;

			STxbuf[datalen++] = hostid;
			STxbuf[datalen++] = MB_WRITE_MUL_COILS;
			STxbuf[datalen++] = RegAdd >> 8;
			STxbuf[datalen++] = RegAdd; // start address to get
			STxbuf[datalen++] = 0;
			STxbuf[datalen++] = 8;
			STxbuf[datalen++] = 1;
			STxbuf[datalen++] = val;
			SMbRxptr = 0;
			SlaveRespons = 0;
			SMbRecBuffer[0] =0;
			MB_Master_sendhost(datalen);

#ifdef BANK_HP
			result = true;
			SlaveRespons =0;
#else
			 	MasterTimeOutTimer = WaitForClientTimeOut;
			 		while(MasterTimeOutTimer >0)
			 		{
			 			if(SlaveRespons && SMbRecBuffer[0] == STxbuf[0] && SMbRecBuffer[1] == STxbuf[1])
			 			{

			 				result = true;
			 				SlaveRespons =0;
			 				break;
			 			}

			 		}
#endif

			 		SlaveRecStat ==	STATE_SLAVE_IDLE;

	return result;

}


uint8_t MBSetSingleCoil(uint8_t hostid,uint16_t RegAdd,uint8_t val)
{
	uint8_t datalen = 0,result=false;
	for(datalen=0;datalen<10;datalen++) SMbRecBuffer[datalen]=0;
		datalen=0;

		STxbuf[datalen++] = hostid;
		STxbuf[datalen++] = MB_WRITE_SINGLE_COIL;
		STxbuf[datalen++] = RegAdd >> 8;
		STxbuf[datalen++] = RegAdd; // start address to get
		STxbuf[datalen++] = val ? 0xff:0;
		STxbuf[datalen++] = 0;
		 	SMbRxptr = 0;
		 	SlaveRespons = 0;
		 	SMbRecBuffer[0] =0;
		 	MB_Master_sendhost(datalen);

#ifdef BANK_HP
			result = true;
			SlaveRespons =0;
#else

		 	MasterTimeOutTimer = WaitForClientTimeOut;
		 		while(MasterTimeOutTimer >0)
		 		{
		 			if(SlaveRespons && SMbRecBuffer[0] == STxbuf[0] && SMbRecBuffer[1] == STxbuf[1])
		 			{

		 				result = true;
		 				SlaveRespons =0;
		 				break;
		 			}

		 		}

#endif
		 		SlaveRecStat ==	STATE_SLAVE_IDLE;

return result;
}


uint8_t MBsetbytercoils(uint8_t hostid,uint16_t RegAdd,uint16_t value)
{
	uint8_t datalen=0,result= false;
	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = 0xfL;
	STxbuf[datalen++] = RegAdd >> 8;
	STxbuf[datalen++] = RegAdd; // start address to get
	STxbuf[datalen++] = 0;
	STxbuf[datalen++] = 8;
	STxbuf[datalen++] = 1;
	STxbuf[datalen++] = value;
 	SMbRxptr = 0;
		 	SlaveRespons = 0;
		 	SMbRecBuffer[0] =0;
		 	MB_Master_sendhost(datalen);

#ifdef BANK_HP
			result = true;
			SlaveRespons =0;
#else

			MasterTimeOutTimer = WaitForClientTimeOut;
				 		while(MasterTimeOutTimer >0)
				 		{
				 			if(SlaveRespons && SMbRecBuffer[0] == STxbuf[0] && SMbRecBuffer[1] == STxbuf[1])
				 			{

				 				result = true;
				 				SlaveRespons =0;
				 				break;
				 			}

				 		}

#endif
	 		SlaveRecStat ==	STATE_SLAVE_IDLE;
		return result;

}


uint16_t MBSetCoilsInputs(uint8_t hostid,uint16_t RegAdd,uint8_t inputs)
{

	uint8_t datalen = 0,cmd = inputs ? MB_INPUTS_CMD : MB_READ_COIL_CMD ;
	uint16_t result = false;

	for(datalen=0;datalen<10;datalen++) SMbRecBuffer[datalen]=0;
	datalen=0;

	STxbuf[datalen++] = hostid;
	STxbuf[datalen++] = cmd;
	STxbuf[datalen++] = RegAdd >> 8;
	STxbuf[datalen++] = RegAdd; // start address to get
	STxbuf[datalen++] = 0;
	STxbuf[datalen++] = 16; //reg count
 	SMbRxptr = 0;
 	SlaveRespons = 0;
 	SMbRecBuffer[0] =0;
 	MB_Master_sendhost(datalen);


	MasterTimeOutTimer = WaitForClientTimeOut;
	while(MasterTimeOutTimer >0)
	{/*********************************************SMbRecBuffer[0] == STxbuf[0]****************/
		if(SlaveRespons &&  SMbRecBuffer[0] == hostid && SMbRecBuffer[1] == cmd)
		{
			result = SMbRecBuffer[3];
			result <<=8;
			result |= SMbRecBuffer[4];

			SlaveRespons = 0;
			break;
		}
			MasterTimeOutTimer--;
			DelayMs(10);
	}



	SlaveRecStat ==	STATE_SLAVE_IDLE;

	return result;
}


volatile char EnableTcpRec = true ;
extern volatile unsigned char eusartRxBuffer[150],eusartRxHead,eusartRxCount,eusartRxTail;

#define STAT_IDEL 0
#define STAT_REC 1
#define STAT_PROC 2
volatile uint8_t TCP_STAT = STAT_IDEL,TcpRecTimer=0;

void TcpTimerCall(void)
{
	if(TCP_STAT ==	STAT_REC)
	{
		if(TcpRecTimer > REC_MSG_INTERVAL)
		{
			TCP_STAT = STAT_PROC;
		}
		else
			TcpRecTimer++;

	}

}



//#define byte uint8_t


        //private byte[]
		uint8_t ModbusTCPRespons()
        {


			uint8_t ValidRespons = 0;
			MasterMode = true;
			int datalen=0,Len=0;
			uint16_t tmp;

			uint8_t result = 0;

			byte respons[60];//
			byte cmd = eusartRxBuffer[7];
            int reqlen = (int)eusartRxBuffer[10] * 0x100 + (int)eusartRxBuffer[11];
            int i;
            for ( i = 0; i < 4; i++)
                respons[i] = eusartRxBuffer[i];

            int responslen = (int)eusartRxBuffer[10] * 0x100 + (int)eusartRxBuffer[11];
            int add = (int)eusartRxBuffer[8] * 0x100 + (int)eusartRxBuffer[9];

            if(cmd == 0x1 || cmd == 0x2) // coil or input command
            {
            	responslen /= 8;
            	if(responslen < 3)
            			responslen = 3;
            }else
            {
            	 responslen *=2;
            	 responslen +=2;
            	 responslen--;
            }


           Len = eusartRxBuffer[5];

             for(i=0;i<Len ;i++)
            		 STxbuf[datalen++] = eusartRxBuffer[6 + i]; // copy modbus req
            Len = 9;

            SMbRxptr = 0;
            SlaveRespons = 0;
            for(i=0;i< 70;i++)
            	  SMbRecBuffer[i] =0;
            MB_Master_sendhost(datalen);
            //for(i=0;i< 50;i++)
            	//	HoldingReg[i] =0;


            MasterTimeOutTimer = WaitForClientTimeOut+200;
			while(MasterTimeOutTimer > 0)
			{/*********************************************SMbRecBuffer[0] == STxbuf[0]****************/
				if(SlaveRespons && SMbRecBuffer[1]  == cmd)// /* &&  SMbRecBuffer[2] == (reqlen*2) */ && responslen < SMbRxptr)// MB_READ_HOLDING_CMD)
				{
					Led(2,ON);
					for(datalen =0;datalen < reqlen;datalen++)
					{
						tmp = 	(uint16_t)SMbRecBuffer[datalen*2 +3] ;
						HoldingReg[datalen] = (tmp  << 8);
						HoldingReg[datalen] += (uint16_t)(SMbRecBuffer[datalen*2 +4]);
					}

					ValidRespons = true;
					result = true;
					SlaveRespons =0;
					break;
				}
					MasterTimeOutTimer--;

					DelayMs(10);
			}





			int tadd =0;


            switch(cmd)//eusartRxBuffer[7])// cmd position
            {

            		case 1: // read coils
                         //   byte Coils[4] = ReportCoilsStata(add, (responslen - 2) / 2);

                           // responslen = Coils.Length + 3;
                        	 responslen /=    8  ;
                        	respons[4] = 0;//(byte)(responslen >> 8); //msg len in bytes
                            respons[5] = SMbRecBuffer[2];//(byte)(responslen & 0xff);
                            respons[6] = SMbRecBuffer[0]; // id
                            respons[7] = SMbRecBuffer[1]; // cmd
                            respons[8] = SMbRecBuffer[2];//  respons len  (byte)responslen;//(Coils.Length);//bytes len

                            responslen = SMbRecBuffer[2];
                            for ( i = 0; i < responslen; i++)
                                respons[i + 9] = SMbRecBuffer[i+3];

                            Len = 9 +responslen;// Coils.Length;


                        break;



                case 3: // read holding



                    respons[4] = (byte)(responslen >> 8); //msg len in bytes
                    respons[5] = (byte)(responslen & 0xff);
                    respons[6] = eusartRxBuffer[6]; // id
                    respons[7] = eusartRxBuffer[7]; // cmd
                    respons[8] = (byte)(responslen - 2);//bytes len

                    for ( i = 0; i < reqlen; i++)
                    {
                      respons[i*2+9] = (byte)(HoldingReg[tadd] >> 8);
                      respons[i*2 + 10] = (byte)(HoldingReg[tadd++] & 0xff);
                      Len += 2;
                    }
                break;



                case 4: // read inputs regs



                                  respons[4] = (byte)(responslen >> 8); //msg len in bytes
                                  respons[5] = (byte)(responslen & 0xff);
                                  respons[6] = eusartRxBuffer[6]; // id
                                  respons[7] = eusartRxBuffer[7]; // cmd
                                  respons[8] = (byte)(responslen - 2);//bytes len

                                  for ( i = 0; i < reqlen; i++)
                                  {
                                    respons[i*2+9] = (byte)(HoldingReg[tadd] >> 8);
                                    respons[i*2 + 10] = (byte)(HoldingReg[tadd++] & 0xff);
                                    Len += 2;
                                  }
                              break;




                case 2: // read input
                    //byte[] Inputs = ReportCoilsStata(add, (responslen - 2) / 2);


                    responslen /=    8  ;
                    respons[4] = 0;//(byte)(responslen >> 8); //msg len in bytes
                    respons[5] = eusartRxBuffer[8];//(byte)(responslen & 0xff);
                    respons[6] = eusartRxBuffer[6]; // id
                    respons[7] = eusartRxBuffer[7]; // cmd
                    respons[8] = eusartRxBuffer[8];//   responslen;//(byte)(Inputs.Length);//bytes len
                    responslen = eusartRxBuffer[8];

                    for ( i = 0; i < responslen; i++)
                        respons[i + 9] = HoldingReg[i];

                    Len = 9 + responslen;
                break;



                case 5: // write signal coil

                    respons[4] = eusartRxBuffer[4]; //msg len in bytes
                    respons[5] = eusartRxBuffer[5];
                    respons[6] = eusartRxBuffer[6]; // id
                    respons[7] = eusartRxBuffer[7]; // cmd
                    respons[8] = eusartRxBuffer[8];//add
                    respons[9] = eusartRxBuffer[9];//add
                    respons[10] = eusartRxBuffer[10];//data
                    respons[11] = eusartRxBuffer[11];//data 00
                    Len = 12 +responslen;

                  //  Coilsreg[tadd] = (eusartRxBuffer[10] == 0xff) ? 1: 0;


                break;

                case 6: //set holding
                                   respons[4] = (byte)(responslen >> 8); //msg len in bytes
                                   respons[5] = (byte)(responslen & 0xff);
                                   respons[6] = eusartRxBuffer[6]; // id
                                   respons[7] = eusartRxBuffer[7]; // cmd
                                   respons[8] = (byte)(responslen - 2);//bytes len

                                //   Holding[add] = (int)eusartRxBuffer[10]<<8;
                                 //  Holding[add] += (int)eusartRxBuffer[11];

                               break;


                default:
                 Len = 2;

                break;




            }

            MasterMode = false;

            if(ValidRespons)
            {

            	for(i=0;i< Len;i++)
            		putcx(respons[i] ,UART1);
            }
            else
            {
            	for(i=0;i< 59;i++)
            	          		respons[i] =0;
            }



            DelayMs(10);

            return result;
        }



void ModBusTcpProc()
{
	int i=0;
	if(TCP_STAT == STAT_PROC)
	{
		EnableTcpRec = false;
		ModbusTCPRespons();
		for(i=0;i< 10;i++)
			eusartRxBuffer[i] =0;
		eusartRxCount =0;
		EnableTcpRec = true;
		TCP_STAT =STAT_IDEL;
	}

}



